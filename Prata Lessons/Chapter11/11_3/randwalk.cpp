#include <iostream>
#include <cstdlib>
#include <ctime>
#include "vect.h"
#include <fstream>

int main()
{
    using namespace std;
    using VECTOR::Vector;
    srand(time(0));
    double direction;
    Vector step;
    Vector result(0.0, 0.0);
    unsigned long steps=0;
    double target;
    double dstep;
    int N;
    int max=0;
    int min=1000;
    int sum=0;

    cout << "Enter number of attempts (q to quit): ";
    while (cin >> N)
    {   
        cout << "Enter target distance: ";
        if (!(cin>>target)){
            break;
        }
        cout << "Enter step length: ";
        if (!(cin >> dstep)){
            break;
        }


        for (int i(0); i<N; i++){
            while (result.magval() < target)
            {   
                direction = rand()%360;
                step.reset(dstep, direction, Vector::POL);
                result = result + step;
                steps++;                
            }
            if (max<steps) max=steps;
            if (min>steps) min=steps;
            sum+=steps;
            steps=0;
            result = Vector(0.0, 0.0);
        }
        cout << endl;
        cout << "For " << N << " attempts: " << endl;
        cout << "Max: " << max << endl;
        cout << "Min: " << min << endl;
        cout << "Average: " << sum/N << endl;
        // cout << "After " << steps << " steps, the subject ";
        // cout << "has the following location:\n";
        // cout << result << endl;
        // result.polar_mode();
        // cout << " or\n" << result << endl;
        // cout << "Average outward distance per step = ";
        // cout << result.magval()/steps << endl;
        // steps = 0;
        // result.reset(0.0, 0.0);
        // cout << "Enter target distance (q to quit): ";
    }
    cout << "Bye!\n";
    cin.clear();
    while (cin.get()!='\n')
    {
        continue;
    }
    cin.get();
    return 0;
}