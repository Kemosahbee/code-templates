#include "complex0.h"

complex::complex()
{
    Re=Im=0;
}

complex::complex(double real, double image)
{
    Re=real;
    Im=image;
}

complex complex::operator+(const complex &n) const
{
    complex sum;
    sum.Re=Re+n.Re;
    sum.Im=Im+n.Im;
    return sum;
}

complex complex::operator-(const complex &n) const
{
    complex diff;
    diff.Re=Re-n.Re;
    diff.Im=Im-n.Im;
    return diff;
}

complex complex::operator*(const complex &n) const
{
    complex mult;
    mult.Re=Re*n.Re-Im*n.Im;
    mult.Im=Re*n.Im+Im*n.Re;
    return mult;
}

complex complex::operator*(const double mult) const
{
    complex mlt;
    mlt.Re=Re*mult;
    mlt.Im=Im*mult;
    return mlt;
}

complex complex::operator~() const
{
    complex pairing;
    pairing.Re=Re;
    pairing.Im=-Im;
    return pairing;
}

complex operator*(const double mult, const complex &n)
{
    return n*mult;
}

std::ostream &operator<<(std:: ostream &os, const complex &n)
{
    os << "(" << n.Re << ", " << n.Im << "i)";
    return os;
}

std::istream &operator>>(std::istream &is, complex &n)
{   
    std::cout << "real: ";
    is >> n.Re;
    std::cout << "imaginary: ";
    is >> n.Im;
    return is;
}