#ifndef COMPLEX0_H_
#define COMPLEX0_H_
#include <iostream>
class complex
{
    private:
        double Re;
        double Im;
    public:
        complex();
        complex(double real, double image);
        ~complex(){};
        complex operator+(const complex &n) const;
        complex operator-(const complex &n) const;
        complex operator*(const complex &n) const;
        complex operator*(const double mult) const;
        complex operator~() const;
        friend complex operator*(const double mult, const complex &n);
        friend std::ostream &operator<<(std::ostream &os, const complex &n);
        friend std::istream &operator>>(std::istream &is, complex &n);
};
#endif