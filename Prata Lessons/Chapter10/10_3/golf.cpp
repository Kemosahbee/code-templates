#include "golf.h"

Golf::Golf(const char *nm, int hc)
{
    strcpy(fullname, nm);
    handicap=hc;
}

int Golf::Setgolf()
{   
    using std::cin;
    using std::cout;
    char fullnm[LEN];
    int hc;
    cout << "Enter fullname: ";
    cin.getline(fullnm, 40);
    if (fullnm[0]){
        cout << "Enter handicap: ";
        cin >> hc;
        Golf tmp(fullnm, hc);
        *this=tmp;
        return 1;
    }else{
        return 0;
    }
}

void Golf::Handicap(int hc)
{
    handicap=hc;
}

void Golf::ShowGolf() const
{
    using std::cout;
    using std::endl;
    cout << "Fullname: " << fullname << "   ";
    cout << "Handicap: " << handicap;
}