#ifndef GOLF_H_
#define GOLF_H_
#include <cstring>
#include <iostream>

class Golf
{
    private:
        static const int LEN=40;
        char fullname[LEN];
        int handicap;
    public:
        Golf()
        {
            strcpy(fullname, "no name");
            handicap=0;
        };
        Golf(const char *nm, int hc);
        int Setgolf();
        void Handicap(int hc);
        void ShowGolf() const;
};
#endif