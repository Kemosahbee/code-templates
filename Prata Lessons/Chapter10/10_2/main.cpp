#include "person.h"

int main()
{   
    Person one;
    Person two("Smythecraft");
    Person three("Dimwiddy", "Sam");
    two.Show();
    two.FormalShow();
    three.Show();
    three.FormalShow();
    one=Person("Savkin", "Konstantin");
    one.Show();
    one.FormalShow();
    std::cin.get();
    return 0;
}