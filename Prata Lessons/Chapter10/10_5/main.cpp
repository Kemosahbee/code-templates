#include "customer.h"
#include <iostream>

double sum=0;
int main()
{   
    using namespace std;
    Stack st;
    char ch;
    customer po;

    cout << "Please enter A to add a purchase order,\n";
    cout << "P to process a PO, or Q to quit.\n";
    while (cin >> ch && toupper(ch)!='Q')
    {
        while (cin.get()!='\n')
        {
            continue;
        }
        if (!isalpha(ch)){
            cout << '\a';
            continue;
        }
        switch(ch)
        {
            case 'A':
            case 'a': 
                cout << "Enter a PO fullname to add: ";
                cin.getline(po.fullname, 35);
                cout << "Enter a PO patment to add: ";
                cin >> po.payment;
                if (st.isfull()){
                    cout << "stack already full\n";
                }else{
                    st.push(po);
                }
                break;
            case 'p':
            case 'P':
                if (st.isempty()){
                    cout << "stack already empty\n"; 
                }else{
                    st.pop(po);
                    cout << "PO #" << po.fullname << "  " << po.payment << " popped\n";
                }
                break;
        }
        cout << "Please enter A to add a purchase order,\n";
        cout << "P to process a PO, or Q to quit.\n";
    }
    cout << "Bye\n";
    return 0;
}