#include "customer.h"
#include <iostream>

Stack::Stack()
{
    top=0;
}

bool Stack::isempty() const
{
    return top==0;
}

bool Stack::isfull() const
{
    return top==MAX;
}

bool Stack::push(const Item &item)
{
    if (top<MAX){
        items[top++]=item;
        return true;
    }else{
        return false;
    }
}
extern double sum;
bool Stack::pop(Item &item)
{
    if (top>0){
        item=items[--top];
        sum+=item.payment;
        std::cout << "Now sum is: " << sum << std::endl;
        return true;
    }else{
        return false;
    }
}

