#include <iostream>
#include "bank.h"

// Bank::Bank() //Конструктор по-умолчанию
// {
//     name="no name";
//     id="no ID";
//     balance=0.0;
// }

Bank::Bank(const std::string &nm, const std::string &nbr, double blnc)
{
    name=nm;
    id=nbr;
    if (blnc<0){
        std::cout << "Balance can't be negative.\n";
        std::cout << "Balance set to 0";
        balance=blnc;
    }else{
        balance=blnc;
    }
}

Bank::~Bank(){}

void Bank::show() const
{
    std::cout << "Holder's name: " << name << std::endl;
    std::cout << "ID: " << id << std::endl;
    std::cout << "Balance: " << balance <<std::endl;
}

void Bank::contribute(double blnc)
{
    if (blnc<0){
        std::cout << "Add amount can't be negative.\n";
        std::cout << "Operation terminated";
    }else{
        balance=balance+blnc;
    }
}

void Bank::take_off(double blnc)
{
    if (blnc<0){
        std::cout << "Withdrawal amount can't be negative.\n";
        std::cout << "Operation terminated";
    }else{
        balance=balance-blnc;
    }
}