#include "bank.h"
#include <iostream>

int main()
{
    Bank Kostya = Bank("Konstantin Savkin", "846", 38500);
    Bank Valentin = Bank("Valentin Balov", "209");
    Bank Ivan;
    Kostya.show();
    Valentin.show();
    Ivan.show();
    Kostya.take_off(12.948);
    Kostya.show();
    Kostya.contribute(525.21);
    Kostya.show();
    Ivan = Bank("Ivan Zaostrovskikh", "007", 15200.0);
    Ivan.show();
    std::cin.get();
    std::cin.get();
    return 0;
}