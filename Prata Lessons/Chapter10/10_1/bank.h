#ifndef BANK_H_
#define BANK_H_
#include <string>

class Bank
{
    private:
        std::string name;
        std::string id;
        double balance;
    public:
        //Конструктор
        // Bank();
        //Создание объекта и его инициализация
        Bank(const std::string &nm="no name", const std::string &nbr="no ID", double blnc=0);
        //Деструктор
        ~Bank();
        //Отображение имени вкладчика, номера счета и баланса
        void show() const;
        //Добавление на счет суммы денег, переданной в аргументе
        void contribute(double blnc);
        //Снятие суммы денег, переданной в аргументе
        void take_off(double blnc);
};
#endif