#ifndef PLORG_H_
#define PLORG_H_
#include <cstring>
class Plorg
{   
    private:
        char name[19];
        int CI;
    public:
        Plorg(const char *nm="I am Plorg!", int idx=50);
        void updCI(int idx);
        void showPlorg() const;
};
#endif