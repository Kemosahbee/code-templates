#include "plorg.h"
#include <iostream>

Plorg::Plorg(const char *nm, int idx)
{
    strcpy(name, nm);
    CI=idx;
}

void Plorg::updCI(int idx)
{
    CI=idx;
}

void Plorg::showPlorg() const
{
    using std::cout;
    using std::endl;
    cout << "My name is " << name << endl;
    cout << "CI: " << CI << endl;
}