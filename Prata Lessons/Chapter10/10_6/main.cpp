#include "move.h"
#include <iostream>

int main()
{
    Move rect1;
    Move rect2(3.1, 5.7);
    rect1.add(rect2);
    rect1.showmove();
    rect2.reset();
    rect2.showmove();
    std::cin.get();
    std::cin.get();
    return 0;
}