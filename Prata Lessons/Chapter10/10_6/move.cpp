#include "move.h"
#include <iostream>

Move::Move(double a, double b)
{
    x=a;
    y=b;
}

void Move::showmove() const
{
    using std::cout;
    using std::endl;
    cout << x << "      " << y << endl;
}

Move Move::add(const Move &m)
{
    (*this).x=m.x;
    (*this).y=m.y;
    return *this;
}

void Move::reset(double a, double b)
{
    x=a;
    y=b;
}