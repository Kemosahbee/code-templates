#include "sales.h"
#include <iostream>

double findMax(const double ar[], int n)
{   
    double max=ar[0];
    for (int i(0); i<n; i++){
        if (max<ar[i]){
            max=ar[i];
        }
    }
    return max;
}

double findMin(const double ar[], int n)
{   
    double min=ar[0];
    for (int i(0); i<n; i++){
        if (min>ar[i]){
            min=ar[i];
        }
    }
    return min;
}

double findAverage(const double ar[], int n)
{   
    double sum=0;
    for (int i(0); i<n; i++){
        sum+=ar[i];
    }
    return sum/n;
}

namespace SALES
{
    Sales::Sales(const double ar[], int n)
    {   
        for (int i(0); i<QUARTERS; i++){
            if (sales[i]==0){
                sales[i]=findMin(ar, n);
                if (i+1<QUARTERS){
                    for (int j=i+1; j<QUARTERS; j++){
                        sales[j]=0;
                    }
                }
            }
            break;
        }
        average=findAverage(ar, n);
        max=findMax(ar, n);
        min=findMin(ar, n);
    }

    void Sales::setSales()
    {   
        using std::cin;
        using std::cout;
        for (int i(0); i<QUARTERS; i++){
            cout << "Enter sales for " << i+1 << " quarter: ";
            cin >> sales[i];
        }
        max=findMax(sales, QUARTERS);
        min=findMin(sales, QUARTERS);
        average=findAverage(sales, QUARTERS);
    }

    void Sales::showSales() const
    {
        using std::endl;
        using std::cout;
        for (int i(0); i<QUARTERS; i++){
            cout << sales[i] << " ";
        }
        cout << endl;
        cout << "Average: " << average << endl;
        cout << "Max: " << max << endl;
        cout << "Min: " << min << endl;
    }
}