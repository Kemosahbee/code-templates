#ifndef SALES_H_
#define SALES_H_
double findMax(const double ar[], int n);
double findMin(const double ar[], int n);
double findAverage(const double ar[], int n);

namespace SALES
{
    class Sales
    {
        private:
            static const int QUARTERS=4;
            double sales[QUARTERS]={0, 0, 0, 0};
            double average;
            double max;
            double min;
        public:
            Sales()
            {
                for (int i(0); i<QUARTERS; i++){
                    sales[i]=0;
                }
                average=0;
                max=0;
                min=0;
            };
            Sales(const double ar[], int n);
            void setSales();
            void showSales() const;            
    };
}
#endif