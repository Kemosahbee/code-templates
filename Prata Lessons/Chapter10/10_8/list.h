#ifndef CUSTOMER_H_
#define CUSTOMER_H_

typedef int Item;
void SQRT(Item&);
void show(Item&);

class List
{
    private:
        static const int MAX=10;
        Item items[MAX];
        int top;
    public:
        List();
        bool isempty() const;
        bool isfull() const;
        bool add(const Item &item);
        void visit(void (*pf)(Item&));
};
#endif