#include "list.h"
#include <iostream>

int main()
{
    using std::cout;
    using std::endl;
    List list;
    list.add(1);
    list.add(2);
    list.add(3);
    list.add(5);
    list.add(10);
    list.add(25);
    list.visit(show);
    list.visit(SQRT);
    list.visit(show);
    list.add(322);
    list.add(228);
    list.visit(show);
    std::cin.get();
    return 0;
}