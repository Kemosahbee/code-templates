#include <iostream>
#include <cstring>
#include "golf.h"

void setgolf(golf & g, const char * name, int hc)
{
    strcpy(g.fullname, name);
    g.handicap=hc;
}

int setgolf(golf & g)
{   
    using namespace std;
    char name[25];
    int hc;
    cout << "Enter fullname: ";
    cin.getline(name, 25);
    if (strlen(name)){
        strcpy(g.fullname, name);
        cout << "Enter handicap: ";
        cin >> hc;
        cin.get();
        g.handicap=hc;
        return 1;
    }else{
        return 0;
    }

}

void handicap(golf & g, int hc)
{
    g.handicap=hc;
}

void showgolf(const golf & g)
{   
    using namespace std;
    cout << "Fullname: " << g.fullname << endl;
    cout << "Handicap: " << g.handicap << endl;
}