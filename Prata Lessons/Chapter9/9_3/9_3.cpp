// Начните со следующего объявления структуры:
// struct chaff
// {
//      char dross[20] ;
//      int slag ;
// } ;
// Напишите программу, которая использует операцию new с размещением, чтобы
// поместить массив из двух таких структур в буфер. Затем программа присваивает
// значения членам структуры (не забудьте воспользоваться функцией s t rcpy ( }
// для массива c h a r ) и отображает ее содержимое с помощью цикла. Вариант 1
// предусматривает применение в качестве буфера памяти статического массива,
// как было показано в листинге 9 . 1 О. Вариант 2 состоит в использовании обычной
// операции new для выделения памяти под буфер.

#include <iostream>
#include <new>
#include <cstring>
using namespace std;

struct chaff
{
    char dross[20];
    int slag;
};
char buffer1[70];

int main()
{
    chaff *p1, *p2;
    cout << "~~~~~~~~Variant 1~~~~~~~~\n";
    p1=new (buffer1) chaff [2];
    strcpy(p1[0].dross, "Hello world ver1");
    p1[0].slag=10;
    strcpy(p1[1].dross, "GOS exam ver1");
    p1[1].slag=5;
    for (int i(0); i<2; i++){
        cout << i+1 << endl;
        cout << p1[i].dross << "    " << p1[i].slag << endl;
    }
    cout << "\n~~~~~~~~Variant 2~~~~~~~~\n";
    p2 = new chaff[2];
    strcpy(p2[0].dross, "Hello world ver2");
    p2[0].slag=12;
    strcpy(p2[1].dross, "GOS exam ver2");
    p2[1].slag=7;
    for (int i(0); i<2; i++){
        cout << i+1 << endl;
        cout << p2[i].dross << "    " << p2[i].slag << endl;
    }
    delete [] p2;
    return 0;
}