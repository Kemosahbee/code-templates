#include <iostream>
#include "sales.h"

namespace SALES
{
    double find_min(const double ar[], int n)
    {
        double min=ar[0];
        for (int i(0); i<n; i++){
            if (min>ar[i]){
                min=ar[i];
            }
        }
        return min;
    }

    double find_max(const double ar[], int n)
    {
        double max=ar[0];
        for (int i(0); i<n; i++){
            if (max<ar[i]){
                max=ar[i];
            }
        }
        return max;
    }

    double find_average(const double ar[], int n)
    {   
        double sum=0;
        for (int i(0); i<n; i++){
            sum+=ar[i];
        }
        return sum/n;
    }

    void setSales(Sales &s, const double ar[], int n)
    {
        for (int i(0); i<QUARTERS; i++){
            if (s.sales[i]==0){
                s.sales[i]=find_min(ar, n);
                if (i+1<QUARTERS){
                    for (int j=i+1; j<QUARTERS; j++){
                        s.sales[j]=0;
                    }
                }
                break;
            }
        }
        s.average=find_average(ar, n);
        s.max=find_max(ar, n);
        s.min=find_min(ar, n);
    }

    void setSales(Sales &s)
    {
        for (int i(0); i<QUARTERS; i++){
            std::cout << i+1 << " Quarter: ";
            std::cin >> s.sales[i];
        }
        s.average=find_average(s.sales, QUARTERS);
        s.max=find_max(s.sales, QUARTERS);
        s.min=find_min(s.sales, QUARTERS);
    }

    void showSales(const Sales &s)
    {
        for (int i(0); i<QUARTERS; i++){
            std::cout << s.sales[i] << "    ";
        }
        std::cout << std::endl;
        std::cout << "Average: " << s.average << std::endl;
        std::cout << "Max: " << s.max << std::endl;
        std::cout << "Min: " << s.min << std::endl;
    }
}