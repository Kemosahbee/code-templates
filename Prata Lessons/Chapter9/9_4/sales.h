double find_min(const double ar[], int n);
double find_max(const double ar[], int n);
double find_average(const double ar[], int n);

namespace SALES
{
    const int QUARTERS=4;
    struct Sales
    {
        double sales[QUARTERS]={0, 0, 0, 0};
        double average;
        double max;
        double min;
    };
    void setSales(Sales &s, const double ar[], int n);
    void setSales(Sales &s);
    void showSales(const Sales &s);

}