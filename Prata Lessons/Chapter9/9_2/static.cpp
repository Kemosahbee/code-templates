// Модифицируйте код в листинге 9.9 , заменив символьный массив объектом
// string. Программа больше не должна проверять, умещается ли вводимая строка,
// и для проверки ввода пустой строки может сравнивать вводимую строку со
// значением " ".

#include <iostream>
#include <string>

void strcount(std::string *str);

int main()
{
    using namespace std;
    string input;
    cout << "Enter a line: \n";
    getline(cin, input);
    while (1)
    {
        strcount(&input);
        cout << "Enter next line (empty line to quit):\n";
        getline(cin, input);
        if (input==""){
            break;
        }
    }
    cout << "Bye\n";
    return 0;
}

void strcount(std::string *str)
{
    using namespace std;
    static int total=0;
    int count=0;
    cout << "\"" << *str << "\" contains ";
    while ((*str)[count])
    {
        count++;
    }
    total+=count;
    cout << count << " characters\n";
    cout << total << " characters total\n";
}