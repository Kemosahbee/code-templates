// Напишите короткую программу, которая запрашивает рост в футах и дюймах и
// вес в фунтах. (Для хранения этой информации используйте три переменных. )
// Программа должна выдать индекс массы тела (body mass index - BMI ) . Чтобы
// рассчитать BMI , сначала преобразуйте рост в футах и дюймах в рост в дюйма.х
// ( 1 фут = 1 2 дюймо в ) . Затем преобразуйте рост в дюймах в рост в метрах, умножив
// на О . 0 2 5 4 . Далее преобразуйте вес в фунтах в массу в килограммах, разделив
// на 2 . 2 . После этого рассчитайте BMI, раздели в массу в килограммах на
// квадрат роста в метрах. Для представления различных коэффициентов преобразования
// используйте символические константы.

#include <iostream>

using namespace std;

int main()
{
    const int Ft_to_inch = 12;
    const double Inch_to_meter = 0.0254;
    const double Lb_to_kg = 2.2;
    int height_ft, height_inch, weight_ft;
    double height_m, weight_kg, BMI;
    cout << "Height foots: ";
    cin >> height_ft;
    cout << "Height inches: ";
    cin >> height_inch;
    cout << "Weight: ";
    cin >> weight_ft;

    height_inch = height_ft*Ft_to_inch+height_inch;
    height_m = height_inch*Inch_to_meter;
    weight_kg = weight_ft/Lb_to_kg;
    BMI = weight_kg/(height_m*height_m);
    cout << "Your height: " << height_m << " meters" << endl;
    cout << "Your weight: " << weight_kg << " kilograms" << endl;
    cout << "Your bodu mass index is: " << BMI;

    return 0;
}