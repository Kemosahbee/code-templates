// Напишите программу, которая запрашивает широту в градусах, минутах и секундах,
// после чего отображает широту в десятичном формате. В одной минуте
// 60 угловых секунд, а в одном градусе 60 угловых минут; представьте эти значения
// с помощью символических констант. Для каждого вводимого значения
// должна использоваться отдельная переменная.

#include <iostream>

using namespace std;

int main()
{
    const double min_to_sec = 60.0;
    const double deg_to_min = 60.0;
    int degrees, minutes, seconds;

    cout << "Enter a latitude in degrees, minutes, and seconds: " << endl;
    cout << "First, enter the degrees: ";
    cin >> degrees;
    cout << "Next, enter the minutes of arc: ";
    cin >> minutes;
    cout << "Finally, enter the seconds of arc: ";
    cin >> seconds;

    cout << degrees << " degrees, ";
    cout << minutes << " minutes, ";
    cout << seconds << " seconds = ";
    cout << degrees+minutes/deg_to_min+seconds/min_to_sec/deg_to_min;
    

    return 0;
}