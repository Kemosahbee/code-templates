// Напишите программу, которая запрашивает текущую численность населения
// Земли и текущую численность населения США ( или любой другой страны).
// Сохраните эту информацию в переменных типа long long. В качестве результата
// программа должна отображать процентное соотношение численности
// населения США ( или выбранной страны ) и всего мира.

#include <iostream>

using namespace std;

int main()
{
    long long population_world, population_USA;
    double percent;

    cout << "Enter the world's population: ";
    cin >> population_world;
    cout << "Enter the population of the US: ";
    cin >> population_USA;

    percent= double(population_USA)/population_world*100;
    cout << "The population of the US is ";
    cout << percent << "% of the world population.";

    return 0;
}