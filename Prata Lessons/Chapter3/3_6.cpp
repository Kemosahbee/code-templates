// Напишите программу, которая запрашивает коли чество миль, пройденных автомобилем,
// и количество галлонов израсходованного бензина, а затем сообщает
// значение количества миль на галлон. Или , если хотите , программа может
// запрашивать расстояние в километрах, а объем бензина в литрах, и выдавать
// результат в виде количества литров на 1 00 километров.

#include <iostream>

using namespace std;

int main()
{
    double gallons, path;
    double percent;

    cout << "Enter gallons: ";
    cin >> gallons;
    cout << "Enter path in kilometers: ";
    cin >> path;

    cout << "The value km/gal. is: ";
    cout << path/gallons;

    return 0;
}