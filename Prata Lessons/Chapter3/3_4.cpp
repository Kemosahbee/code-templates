// Напишите программу, которая запрашивает коли чество секунд в виде целого
// значения (используйте тип long или long long, если последни й доступен) и
// затем отображает эквивалентное значение в сутках, часах, минутах и секундах.
// Для представления количества часов в сутках, количества минут в часе и количества
// секунд в минуте используйте символические константы.

#include <iostream>

using namespace std;

int main()
{
    long long sec;
    const int day_to_hour = 24;
    const int hour_to_min = 60;
    const int min_to_sec = 60;

    cout << "Enter the number of seconds: ";
    cin >> sec;

    cout << sec << " seconds = ";
    cout << sec/day_to_hour/hour_to_min/min_to_sec << " days, ";
    sec = sec%(day_to_hour*hour_to_min*min_to_sec);
    cout << sec/(hour_to_min*min_to_sec) << " hours, ";
    sec=sec%(hour_to_min*min_to_sec);
    cout << sec/min_to_sec << " minutes, ";
    cout << sec%min_to_sec << " seconds";
    

    return 0;
}