#include "stack.h"
#include <iostream>

int main()
{
    using namespace std;
    Stack st(5);
    char ch;
    unsigned long po;
    cout  << "please enter A to add a purchase order, \n";
    cout << "P to process a PO, or Q to quit.\n";
    while (cin>>ch && toupper(ch)!='Q')
    {
        while (cin.get()!='\n')
        {
            continue;
        }
        if (!isalpha(ch)){
            cout <<'\a';
            continue;
        }
        switch(ch)
        {
            case 'A':
            case 'a':
            {
                cout << "Enter a PO number to add: ";
                cin >> po;
                if (st.isfull()){
                    cout << "stack is already full\n";
                }else{
                    st.push(po);
                }
                break;
            }
            case 'P':
            case 'p':
            {
                if (st.isempty()){
                    cout << "stack already empty\n";
                }else{
                    st.pop(po);
                    cout << "PO #" << po << " popped\n";
                }
                break;
            }
        }
        cout  << "please enter A to add a purchase order, \n";
        cout << "P to process a PO, or Q to quit.\n";
    }
    unsigned long po1;
    Stack st1(5);
    st1=st;
    st1.pop(po1);
    cout << "PO #" << po1 << " popped\n";
    st1.pop(po1);
    cout << "PO #" << po1 << " popped\n";
    st1.pop(po1);
    cout << "PO #" << po1 << " popped\n";

    Stack st2(st1);
    st2.pop(po1);
    cout << "PO #" << po1 << " popped\n";
    st2.pop(po1);
    cout << "PO #" << po1 << " popped\n";
    cout << "Bye\n";
    cin.get();
    cin.get();
    return 0;
}