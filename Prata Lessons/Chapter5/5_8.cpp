// Напишите программу, которая использует массив c h a r и цикл для чтения по
// одному слову за раз до тех пор, пока не будет введено слово done. Затем программа
// должна сообщить количество введенных слов (исключая done ) .

#include <iostream>
#include <cstring>
 
int main()
{
    using namespace std;
    char word[10];
    int i = 0;
    int count = 0;
    cout << "Enter any words (enter done to quit): ";
    do
    {
          cin >> word;
          ++count;
    }
          while(strcmp(word, "done") != 0);
          --count;
    cout << "You entered " << count << " words except done.";
    cin.get();
    cin.get();
    return 0;
}