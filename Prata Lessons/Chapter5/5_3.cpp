// Напишите программу, которая приглашает пользователя вводить числа. После
// каждого введенного значения программа должна выдавать накопленную сумму
// введенных значений. Программа должна завершаться при вводе О .

#include <iostream>

using namespace std;

int main()
{   
    int a, sum=0;
    cout << "Enter number. 0 - exit" << endl;

    while (a != 0)
    {
        cin >> a;
        sum += a;
        cout << "Sum of entered numbers = " << sum << endl;
    }
    
    return 0;
}