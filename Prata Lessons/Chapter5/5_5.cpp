// Предположим, что вы продаете книгу по программированию на языке С++ для
// начинающих. Напишите программу, которая позволит ввести ежемесячные
// объемы продаж в течение года ( в количестве книг, а не в деньгах) . Программа
// должна использовать цикл , в котором выводится приглашение с названием месяца,
// применяя массив указателей на char ( или массив объектов s t r i n g , если
// вы предпочитаете его) , инициализированный строками - названиями месяцев,
// и сохраняя введенные значения в массиве int. Затем программа должна найти
// сумму содержимого массива и выдать общий объем продаж за год.

#include <iostream>
#include <string>

using namespace std;

int main()
{
    string month[12]={"january", "february", "march",
                     "april", "may", "june",
                     "july", "august", "september",
                     "october", "november", "december"};

    int sales[12], TotalAmount=0;

    for (int i(0); i<12; i++)
    {
        cout << month[i] << " sales: ";
        cin >> sales[i];
        TotalAmount+=sales[i];
    }
    cout << "Total number of books sold: " << TotalAmount;
    return 0;
}