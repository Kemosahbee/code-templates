// Напишите программу, соответствующую описанию программы из упражнения 8,
// но с использованием объекта s t r i n g вместо символьного массива. Включите
// заголовочный файл s t r i ng и применяйте операции отношений 􁕕ля выполнения
// проверки.

#include <iostream>
#include <string>

int main()
{
    using namespace std;
    string word;
    int count = 0;
    cout << "Enter any words (enter done to quit): ";
    do
    {
          cin >> word;
          ++count;
    }
          while(word != "done");
          --count;
    cout << "You entered " << count << " words except done.";
    cin.get();
    cin.get();
    return 0;
}