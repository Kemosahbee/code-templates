// Напишите программу, запрашивающую у пользователя ввод двух целых чисел.
// Затем программа должна вычислить и выдать сумму всех целых чисел , лежащих
// между этими двумя целыми. Предполагается , что меньшее значение вводится
// первым. Например, если пользователь ввел 2 и 9, программа должна сообщить ,
// что сумма всех целых чисел от 2 до 9 равна 44.

#include <iostream>
#include <string>
#include <cstring>

using namespace std;

int main()
{

    cout << "Enter two integer numbers" << endl;
    int nbr1, nbr2, sum=0;
    cout << "Number 1: ";
    cin >> nbr1;
    cout << "Number 2: ";
    cin >> nbr2;

    for (int i(nbr1); i<=nbr2; i++)
    {
        sum+=i;
    }

    cout << "Sum between this numbers: " << sum;
    return 0;
}