// Выполните упражнение 5 , но используя двумерный массив для сохранения данных
// о месячных продажах за 3 года. Выдайте общую сумму продаж за каждый
// год и за все годы вместе.

#include <iostream>
#include <string>

using namespace std;

int main()
{
    string month[12]={"january", "february", "march",
                     "april", "may", "june",
                     "july", "august", "september",
                     "october", "november", "december"};

    int sales[3][12], TotalAmount=0, YearAmount[3];

    for (int j(0); j<3; j++)
    {   
        YearAmount[j] = 0;
        for (int i(0); i<12; i++)
        {
            cout << month[i] << " sales: ";
            cin >> sales[j][i];
            YearAmount[j]+=sales[j][i];
        }
        cout << "Total number of books sold in year " << j+1 << " is " << YearAmount[j] << endl;
        TotalAmount = TotalAmount + YearAmount[j];
    }
    cout << "Total number of books sold: " << TotalAmount;
    return 0;
}