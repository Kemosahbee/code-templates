// Перепишите код из листинга 5.4 с использованием объекта array вместо встроенного
// массива и типа l ong douЫe вместо l o ng l ong. Найдите значение 100!

#include <iostream>
#include <array>

using namespace std;
const int ArSize = 100;

int main()
{
    array<long double, ArSize> factorials;

    factorials[0]=1;
    factorials[1]=1;

    for (int i(2); i<ArSize; i++)
    {
        factorials[i]=i*factorials[i-1];
    }

    for (int i(0); i<ArSize; i++)
    {
        cout << i << "! = " << factorials[i] << endl;
    }
    return 0;
}