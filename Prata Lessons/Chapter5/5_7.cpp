// Разработайте структуру по имени c a r , которая будет хранить следующую информацию
// об автомобиле: название производителя в виде строки в символьном
// массиве или в объекте s t r i n g , а также год выпуска автомобиля в виде целого
// числа. Напишите программу, которая запросит пользователя, сколько автомобилей
// необходимо включить в каталог. Затем программа должна применить new
// для создания динамического массива структур car указанного пользователем
// размера. Далее она должна пригласить пользователя ввести название производителя
// и год выпуска для наполнения данными каждой структуры в массиве (см.
// главу 4 ) . И, наконец, она должна отобразить содержимое каждой структуры.

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

struct car
{
    string CarName;
    int CarYear;
};

int main()
{
    cout << "How many cars you want enter?" << endl;
    int CarAmount;
    (cin >> CarAmount).get();

    car *CarList = new car[CarAmount];

    for (int i(0); i<CarAmount; i++)
    {
        cout << "Automobile #" << i+1 << endl;
        cout << "Enter manufacturer: ";
        getline(cin, CarList[i].CarName);
        cout << "Year of issue: ";
        (cin >> CarList[i].CarYear).get();
    }

    system("cls");

    cout << "Here your collection:" << endl;
    for (int i(0); i<CarAmount; i++)
    {
        cout << CarList[i].CarYear << "  " << CarList[i].CarName << endl;
    }

    delete [] CarList;

    return 0;
}