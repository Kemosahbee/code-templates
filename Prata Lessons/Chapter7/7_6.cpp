// Напишите программу, использующую описанные ниже функции .
// Fill_array( ) принимает в качестве аргумента имя массива элементов типа
// douЬle и размер этого массива. Она приглашает пользователя
// ввести значения douЫe для помещения их в массив. Ввод
// прекращается при наполнении массива либо когда пользователь
// вводит нечисловое значение и возвращает действительное
// количество элементов.
// Show_array() принимает в качестве аргументов имя массива значений douЫe,
// а также его размер, и отображает содержимое массива.
// Reverse_array( ) принимает в качестве ар гумента имя массива значений
// douЬle , а также его размер , и изменяет порядок его элементов
// на противоположный.
// Программа должна использовать эти функции для наполнения массива, обращения
// порядка его элементов, кроме первого и пос:11еднего, с последующим
// отображением.

#include <iostream>

using namespace std;
const int length=7;
int Fill_array(double ar[], int size);
void Show_array(const double ar[], int size);
void Reverse_array(double ar[], int size);

int main()
{   
    double array[length];
    int size=Fill_array(array, length);
    Show_array(array, size);

    Reverse_array(array, size);

    double temp;
    temp=array[0];
    array[0]=array[size-1];
    array[size-1]=temp;
    
    Show_array(array, size);
    return 0;
}

int Fill_array(double ar[], int size)
{   
    cout << "~~~Array filling~~~" << endl;
    int i;
    double temp;
    for (i=0; i<size; i++){
        cout << "Element #" << i+1 << ": ";
        cin >> temp;
        if (!cin){
            cin.clear();
            while (cin.get()!='\n')
            {
                continue;
            }
            cout << "Input terminated" << endl;
            break;
        }
        ar[i]=temp;
    }
    return i;
}

void Show_array(const double ar[], int size)
{   
    cout << "~~~Show array~~~" << endl;
    for (int i(0); i<size; i++){
        cout << "Element #" << i+1 << " = " << ar[i] << endl;
    }
}

void Reverse_array(double ar[], int size)
{
    double temp;
    for (int i=0, j=(size-1); i<size/2; i++, j--){
        temp=ar[i];
        ar[i]=ar[j];
        ar[j]=temp;
    }

}