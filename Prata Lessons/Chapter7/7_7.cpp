// Вернитесь к программе из листинга 7 . 7 и замените три функции обработки массивов
// версиями , которые работают с диапазонами значений , заданными парой
// указателей. Функция f i l l _ a rray ( ) вместо возврата действительного количества
// прочитанных значений должна возвращать указатель на место , следующее за
// последним введенным элементом; прочие функции должны использовать его в
// качестве второго аргумента для идентификации конца диапазона данных.

#include <iostream>
#include <conio.h>
using namespace std;
double *fill_array(double *begin, double *end);
void show_array(const double *begin, const double *end);
void revalue(double *begin, double *end, double r);

int main()
{   
    double ar[7];
    double *end;
    end=fill_array(ar, ar+7);
    system("cls");
    show_array(ar, end);
    cout << endl;
    cout << endl;
    double r=2.0;
    revalue(ar, end, r);
    cout << "---NEW-ARRAY---" << endl;
    show_array(ar, end);
    return 0;
}

double *fill_array(double *begin, double *end)
{
    double *pt;
    double temp;
    cout << "~~~Fill array~~~" << endl;
    for (pt=begin; pt!=end; pt++){
        cout << "Element #" << pt-begin+1 << ": ";
        cin >> temp;
        if (!cin){
            cin.clear();
            while (cin.get()!='\n')
            {
                continue;
            }
            cout << "Input terminated" << endl;
            break;
        }
        *pt=temp;
    }
    return pt;
}

void show_array(const double *begin, const double *end)
{   
    const double *pt;
    cout << "~~~Show array~~~" << endl;
    for (pt=begin; pt!=end; pt++){
        cout << "Element #" << pt-begin+1 << ": " << *pt << endl;
    }
}

void revalue(double *begin, double *end, double r)
{
    for (double *pt=begin; pt!=end; pt++){
        *pt=*pt*r;
    }
}