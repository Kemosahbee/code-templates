// Напишите программу, которая запрашивает у пользователя 1 0. результатов игры
// в гольф , сохраняя их в массиве. При этом необходимо обеспечить возможность
// прекращения ввода до ввода всех 10 результатов. Программа должна отобразить
// все результаты в одной строке и сообщить их среднее значение. Реализуйте
// ввод, отображение и вычисление среднего в трех отдельных функциях, работающих
// с массивами.

#include <iostream>
#include <conio.h>

using namespace std;

const int Max=10;

int input(double ar[], int n)
{   
    double temp;
    int i;
    for (i=0; i<n; i++){
        cout << "Enter value #" << (i+1) << ": ";
        cin >> temp;
        if (!cin){
            cin.clear();
            while (cin.get()!='\n')
            {
                continue;
            }
            cout << "Bad input; input process terminated." << endl;
            break;
        }else if (temp < 0){
            break;
        }
        ar[i]=temp;
    }
    return i;
}

void show_array(const double ar[], int n)
{   
    cout << "~~~Your array~~~" << endl;
    for (int i(0); i<n; i++){
        cout << "Element #" << i+1 << " " << ar[i] << endl;
    }
}

double average(double ar[], int n)
{   
    double sum=0.0;
    for (int i(0); i<n; i++){
        sum+=ar[i];
    }
    return sum/n;
}

int main()
{
    double array[Max];
    int size;
    size=input(array, Max);
    system("cls");
    if (size==0){
        cout << "No values" << endl;
    }else{
        show_array(array, size);
        cout << "Average = " << average(array, size) << endl;
    }
    return 0;
}

