// Определите рекурсивную функцию, принимающую целый аргумент и возвращающу19
// его факториал. Вспомните , что факториал 3 записывается, как 3! и
// вычисляется как 3 х 2 ! и т.д. , причем О! равно 1 . В общем случае , если n больше
// нуля , то n!=n*(n-1)! . Протестируйте функцию в программе, использующей
// цикл , где пользователь может вводить различные значения, для которых программа
// вычисляет и отображает факториалы.

#include <iostream>

using namespace std;
unsigned long long factorial(int n);

int main()
{   
    int n;
    cout << "Enter a number to find factorial: ";
    while ((cin >> n) && n>=0)
    {
        cout << n << "! = " << factorial(n) << endl;
        cout << "Enter another number: ";
    }

    return 0;
}

unsigned long long factorial(int n)
{
    unsigned long long result=n;
    if (n>=1){
        result=result*factorial(n-1);
    }else if (n==0){
        result=1;
    }
    return result;
}