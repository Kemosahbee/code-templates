// Многие лотереи в CIIIA организованы подобно той , что была смоделирована
// одного набора, называемого полем номеров. ( Например, вы можете выбрать 5
// чисел из поля 1-47. ) Вы также указываете один номер (называемый мсганомером)
// из второго диапазона , такого как 1-27. Чтобы выиграть главный приз, вы
// должны правильно угадать все номера. Шанс выиграть вычисляется как вероятность
// угадывания всех номеров в поле, умноженная на вероятность угадывания
// меганомера. Например, вероятность выигрыша в описанном здесь примере
// вычисляется как вероятность угадывания 5 номеров из 4 7, умноженная на
// вероятность угадывания одного номера из 27. Модифицируйте листинг 7.4 для
// вычисления вероятности выигрыша в такой лотерее.

#include <iostream>

using namespace std;

long double probability(unsigned, unsigned);

int main()
{
    double total, choices, megaTotal, megaChoice=1;
    cout << "Enter the total number of choices on the game card and\n";
    cout << "the number of picks allowed and total numbers for mega choice:\n";
    while ((cin >> total >> choices >> megaTotal) && choices <= total && megaTotal<=total)
    {
        cout << "You have one chance in ";
        cout << probability(total, choices)*probability(megaTotal, 1);
        cout << " of winning.\n";
        cout << "Next two numbers (q to quit): ";
    }
    cout << "bye\n";
    return 0;
}

long double probability(unsigned numbers, unsigned picks)
{
    long double result=1.0;
    long double n;
    unsigned int p;
    for (n=numbers, p=picks; p>0; n--, p--){
        result=result*n/p;
    }
    
    return result;
}
