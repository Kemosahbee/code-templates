// Пусrь имеется следующее объявление структуры:
// struct Ьох
// {
// } ;
// char maker[40] ;
// float height ;
// float width ;
// float length· ;
// float volume ;
// а. Напишите функцию , принимающую структуру Ьох по значению и отображающую
// все ее члены.
// б. Напишите функцию , принимающую адрес структуры Ьох и устанавливающую
// значение члена vol ume равным произведению остальных трех членов.
// в. Напишите простую программу, которая использует эти две функции.

#include <iostream>
#include <conio.h>
using namespace std;

struct box
{
    char maker[40];
    float height;
    float width;
    float length;
    float volume;
};

void aFunc(const box a)
{
    cout << "Maker: " << a.maker << endl;
    cout << "Height: " << a.height << endl;
    cout << "Width: " << a.width << endl;
    cout << "Length: " << a.length << endl;
    cout << "Volume: " << a.volume << endl;
}

void bFunc(box *b)
{
    b->volume=b->height*b->width*b->length;
}

int main()
{
    box *randomBox = new box;
    cout << "Maker: ";
    cin.getline(randomBox->maker, 40);
    cout << "Height: ";
    cin >> randomBox->height;
    cout << "Width: ";
    cin >> randomBox->width;
    cout << "Length: ";
    cin >> randomBox->length;

    bFunc(randomBox);
    system("cls");
    aFunc(*randomBox);

    delete randomBox;
    return 0;
}
