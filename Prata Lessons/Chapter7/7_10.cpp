#include <iostream>

using namespace std;

double calculate(double, double, double (*func)(double, double));
double add(double, double);
double sub(double, double);
double mult(double, double);
double divide(double, double);

int main()
{
    cout << "Enter two numbers (q to exit): ";
    double a, b;
    while(cin >> a >> b)
    {
        cout << "Add: " << calculate(a, b, add) << endl;
        cout << "Sub: " << calculate(a, b, sub) << endl;
        cout << "Mult: " << calculate(a, b, mult) << endl;
        cout << "Divide: " << calculate(a, b, divide) << endl;
        cout << "Enter another numbers: ";
    }
    return 0;
}


double add(double a, double b)
{
    return a+b;
}

double sub(double a, double b)
{
    return a-b;
}

double mult(double a, double b)
{
    return a*b;
}

double divide(double a, double b)
{
    return a/b;
}

double calculate(double a, double b, double (*func)(double, double))
{
    double result = (*func)(a, b);
    return result;
}