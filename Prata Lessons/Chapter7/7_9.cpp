// Следующее упражнение позволит попрактиковаться в написании функций , работающих
// с массивами и струкТурами. Ниже представлен каркас программы.
// Дополните его функциями , описанными в комментариях.

#include <iostream>
#include <cstring>
using namespace std;

const int SLEN = 30;
struct student
{
    char fullname[SLEN];
    char hobby[SLEN];
    int ooplevel;
};

int getinfo(student pa[], int n);
void display1(student st);
void display2(const student *ps);
void display3(const student pa[], int n);

int main()
{
    cout << "Enter class size: ";
    int class_size;
    cin >> class_size;
    cin.get();
    // while (cin.get()!='\n')
    // {
    //     continue;
    // }
    student * ptr_stu = new student[class_size];
    int entered = getinfo(ptr_stu, class_size);
    cout << entered << endl;
    for (int i(0); i<entered; i++){
        display1(ptr_stu[i]);
        display2(&ptr_stu[i]);
    }
    display3(ptr_stu, entered);
    delete [] ptr_stu;
    cout << "Done\n";
    return 0;
}

int getinfo(student pa[], int n)
{
    int i;
    student temp;
    for (i=0; i<n; i++){
        cout << "~~~STUDEENT #" << i+1 << ": " << endl;
        cout << "Enter fullname: ";
        cin.getline(temp.fullname, SLEN);
        if (!strlen(temp.fullname)){
            cout << "Input terminated";
            break;
        }
        cout << "Enter hobby: ";
        cin.getline(temp.hobby, SLEN);
        cout << "Enter ooplevel: ";
        while (!(cin >> temp.ooplevel))
        {
            cin.clear();
            while (cin.get()!='\n')
            {
                continue;
            }
            cout << "Invalid value. Please repeat ooplevel input: ";
        }
        cin.get();
        pa[i]=temp;
    }
    return i;
}


void display1(const student st)
{
    cout << "~~~(display1)~~~" << endl;
    cout << "Fullname: " << st.fullname;
    cout << "    hobby: " << st.hobby;
    cout << "   ooplevel: " << st.ooplevel << endl;
}

void display2(const student *ps)
{
    cout << "~~~(display2)~~~" << endl;
    cout << "Fullname: " << ps->fullname;
    cout << "    hobby: " << ps->hobby;
    cout << "   ooplevel: " << ps->ooplevel << endl;
}

void display3(const student pa[], int n)
{   
    for (int i(0); i<n; i++){
        cout << "~~~STUDENT #" << i+1 << " ~~~" << endl;
        cout << "Fullname: " << pa[i].fullname;
        cout << "    hobby: " << pa[i].hobby;
        cout << "   ooplevel: " << pa[i].ooplevel << endl;
    }

}