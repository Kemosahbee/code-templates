// Вернитесь к программе из листинга 7.15 , не использующей класс array. Напишите
// следующие две версии.
// а. Используйте обычный массив из const char * для строковых представлений
// времен года и обычный массив из douЫe для расходов.
// б. Используйте обычный массив из const char * для строковых представлений
// времен года и структуру, единственный член которой является обычным
// массивом из douЫe для расходов. (Это очень похоже на базовое проектное
// решение для класса array.)

#include <iostream>

using namespace std;

const int Seasons=4;
const char *seasons[Seasons] = {"Spring", "Summer", "Fall", "Winter"};

void fill(double expenses[]);
void show(const double expenses[]);

int main()
{   
    double expenses[Seasons];
    fill(expenses);
    show(expenses);

    return 0;
}

void fill(double expenses[])
{
    for (int i=0; i<Seasons; i++){
        cout << "Enter " << seasons[i] << " expenses: ";
        cin >> expenses[i];
    }
}

void show(const double expenses[]){
    double total =0.0;
    cout << "\nEXPENSES\n";
    for (int i=0; i<Seasons; i++){
        cout << seasons[i] << ": $" << expenses[i] << endl;
        total+=expenses[i];
    }
    cout << "Total Expenses: $" << total << endl;
}