// Напишите программу, которая многократно запраши вает у пользователя пару
// чисел до тех пор, пока хотя бы одно из этой п ары не будет равно О . С каждой
// парой программа должна использовать функцию для вычисления среднего
// гармонического этих чисел. Функция должна возвращать ответ ma i n ( ) для отображения
// результата. Среднее гармоническое чисел - это инверсия среднего
// значения их инверсий

#include <iostream>

using namespace std;
double Harmonic_number_mean(double, double);

int main()
{
    double a, b;
    cout << "Enter two numbers: ";
    cin >> a >> b;
    while (a && b)
    {   
        cout << "Harmonic number mean = ";
        cout << Harmonic_number_mean(a, b) << endl;
        cout << "Enter another two numbers: ";
        cin >> a >> b;
    }
    return 0;
}

double Harmonic_number_mean(double a, double b)
{
    return 2.0*a*b/(a+b);
}