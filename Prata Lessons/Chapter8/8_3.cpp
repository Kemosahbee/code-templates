// Напишите функцию, которая принимает ссылку на объект s t r i n g в качестве
// параметра и преобразует содержимое s t r i n g в символы верхнего регистра.
// Используйте функцию touppe r ( ) , описанную в табл . 6.4 (см. главу 6). Напишите
// программу, использующую цикл, которая позволяет проверить работу функции
// для разного ввода.

#include <iostream>
#include <string>
#include <cctype>

using namespace std;

void toup(string &str);


int main()
{   
    cout << "Enter a string (q to quit): ";
    string str;
    getline(cin, str);
    while (str!="q")
    {
        toup(str);
        cout << str << endl;
        cout << "Next string (q to quit): ";
        getline(cin, str);
    }
    cout << "Bye";
    return 0;
}

void toup(string &str)
{   
    int i=0;
    while (str[i])
    {
        str[i]=toupper(str[i]);
        i++;
    }
}