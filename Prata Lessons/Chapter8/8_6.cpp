// Напишите шаблонную функцию maxn ( ) , которая принимает в качестве аргумента
// массив элементов типа т и целое число, представляющее количество элементов
// в массиве, а возвращает элемент с наибольшим значением. Протестируйте
// ее работу в программе, которая использует этот шаблон с массивом из шести
// значений int и массивом из четырех значений douЫ e . Программа также должна
// включать специализацию, которая использует массив указателей на cha r в качестве
// первого аргумента и количество указателей - в качестве второго , а затем
// возвращает адрес самой длинной строки. Если имеется более одной строки наибольшей
// длины, функция должна вернугь адрес первой из них. Протестируйте
// специализацию на массиве из пяти указателей на строки .

#include <iostream>
#include<cstring>
using namespace std;

template <typename T>
T maxn(const T *array, int n);

const char *maxn(const char *ar[], int n);

// void maximum(const char *ar[], int n){
//     int max=strlen(ar[0]);
//     for (int i(0); i<n-1; i++){
//         if (max<strlen(ar[i+1])){
//             max=strlen(ar[i+1]);
//         }
//     }
//     cout << max << endl;
// }

int main()
{
    int ar1[6]={20, 10, 40, 0, -50, 3};
    double ar2[4]={14.88, 2.28, 32.2, 6.9};
    const char *ar[3]={"Prata lesson", "C++ learning", "Hello world"};
    cout << maxn(ar1, 6) << endl;
    cout << maxn(ar2, 4) << endl;
    // maximum(ar,3);
    cout << maxn(ar, 3);
    return 0;
}

template <typename T>
T maxn(const T *array, int n)
{   
    T max=array[0];
    for (int i(0); i<n-1; i++){
        if (max<array[i+1])
        {
            max=array[i+1];
        }
    }
    return max;
}

const char *maxn(const char *ar[], int n)
{
    int max=strlen(ar[0]);

    for (int i(0); i<n-1; i++){
        if (max<strlen(ar[i+1])){
            max=strlen(ar[i+1]);
        }
    }

    for (int i(0); i<n; i++){
        if (strlen(ar[i])==max){
            return ar[i];
            break;
        }
    }
}
