#include <iostream>
#include <cstring>

using namespace std;

struct stringy
{
    char *str;
    int ct;
};

void show(const stringy &, int n=1);
void show(const char*, int n=1);
void set(stringy &, const char*);

int main()
{
    stringy beany;
    char testing[]="Reality isn't what it used to be.";
    set(beany, testing);
    show(beany);
    show(beany, 2);
    testing[0]='D';
    testing[1]='u';
    show(testing);
    show(testing, 3);
    show("Done!");
    return 0;
}

void show(const stringy &a, int n)
{
    for (int i(0); i<n; i++){
        cout << a.str << endl;;
    }
}

void show(const char* str, int n)
{
    for (int i(0); i<n; i++){
        cout << str << endl;
    }
}

void set(stringy &a, const char *str)
{
    a.str = new char[strlen(str)+1];
    strcpy(a.str, str);
    a.ct=strlen(str);
}