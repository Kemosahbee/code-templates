// Напишите шаблонную функцию maxS ( ) , которая принимает в качестве аргумента
// массив из пяти элементов типа т и возвращает наибольший элемент в массиве.
// ( Поскольку размер массива фиксирован, его можно жестко закодировать в
// цикле, а не передавать в виде аргумента . ) Протестируйте функцию в программе
// с использованием массива из пяти значений int и массива из пяти значений
// douЫ e .

#include <iostream>

using namespace std;

template <typename T>
T max5(const T *array);

int main()
{
    int ar1[5]={1, 4, 8, 19, 0};
    double ar2[5]={20.2, 10.1, 5.0, 1.12, 19};
    cout << max5(ar1) << endl;
    cout << max5(ar2) << endl;
    return 0;
}

template <typename T>
T max5(const T *array)
{   
    T max=array[0];
    for (int i(0); i<4; i++){
        if (max<array[i+1])
        {
            max=array[i+1];
        }
    }
    return max;
}