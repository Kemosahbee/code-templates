// Структура CandyB ar содержит три члена. Первый член хранит название коробки
// конфет. Второй - ее вес ( который может иметь дробную часть) , а третий - коли
// чество калорий ( целое значение) . Напишите программу, использующую функцию,
// которая принимает в качестве аргументов ссылку на Ca ndyB a r , указатель
// на c h a r , значение douЫ e и значение i n t . Функция использует три последних
// значения для установки соответствующих членов структуры . Три последних аргумента
// должны иметь значения по умолчанию: "Mi l l enn ium Mun c h " , 2 . 85 и 3 5 0 .
// Кроме того , программа должна использовать функцию, которая принимает в качестве
// аргумента ссылку на CandyBar и отображает содержимое этой структуры.
// Где необходимо, используйте c o n s t .

#include <iostream>
#include <cstring>

using namespace std;

struct CandyBar
{
    char name[20];
    double weight;
    int ccals;
};

void fill(CandyBar &a, const char* str="Millennium Munch", const double weight=2.85, const int ccals=350);
void show(const CandyBar &a);

int main()
{
    CandyBar candy;
    fill(candy);
    show(candy);
    fill(candy, "Krasny Oktober", 0.250, 600);
    show(candy);
    return 0;
}

void fill(CandyBar &a, const char* str, const double weight, const int ccals)
{
    strcpy(a.name, str);
    a.weight=weight;
    a.ccals=ccals;
}

void show(const CandyBar &a)
{
    cout << "Name: " << a.name << endl;
    cout << "Weight: " << a.weight << endl;
    cout << "ccals: " << a.ccals << endl;
    cout << endl;
}
