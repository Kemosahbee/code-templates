// Напишите программу, которая запрашивает у пользователя имя , фамилию, а
// затем конструирует, сохраняет и отображает третью строку, состоящую из фамилии
// пользователя, за которой следует запятая, пробел и его имя. Используйте
// массивы c h a r и функции из заголовочного файла c s t r i n g .

// Напишите программу, которая приглашает пользователя ввести е г о имя и фамили
// ю , а затем построит, сохранит и отобразит третью строку, состоящую
// из фамили и , за которой следует запятая, пробел и имя. Используйте объекты
// s t r i n g и методы из заголовочного файла s t r ing.

#include <iostream>
#include <cstring>
#include <string>

using namespace std;

int main()
{
    string string_first_name, string_last_name, string_whole_name;
    cout << "Enter your first name: \n";
    getline(cin, string_first_name);
    cout << "Enter your last name: \n";
    getline(cin, string_last_name);
    string_whole_name = string_last_name + ", " + string_first_name;
    cout << "Here's the information in a single string:  " << string_whole_name << endl;

    char char_first_name[20], char_last_name[20];
    cout << "Enter your first name: \n";
    cin.getline(char_first_name, 20);
    cout << "Enter your last name: \n";
    cin.getline(char_last_name, 20);
    char *char_whole_name = new char[strlen(char_first_name)+strlen(char_last_name)+4];
    strcpy(char_whole_name,  char_last_name);
    strcat(char_whole_name, ", ");
    strcat(char_whole_name, char_first_name);
    cout << "Here's the information in a single string:  " << char_whole_name << endl;
    return 0;
}