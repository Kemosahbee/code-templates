// Напишите программу С++, которая запрашивает и отображает информацию,
// как показано в следующем примере вывода:
// What i s your f i r s t n ame ? B e t t y S u e
// What i s y o u r l a s t n a me ? Y e w e
// What l e t t e r g r a d e do уои de s e rve ? В
// What i s y o u r age ? 2 2
// Name : Yewe , B e t t y S u e
// Grade : С
// Age : 2 2
// Обратите вниман и е , что программа должна принимать имена, состоящие из
// более чем одного слова. Кроме того, программа должна уменьшать значение
// g rade на одну градацию - т.е. на одну букву выше. Предполагается, что пользователь
// может ввести А, В или С, поэтому вам не нужно беспокоиться о пропуске
// между D и F.

#include <iostream>

using namespace std;

int main()
{   
    char first_name[20], last_name[20], grade;
    int age;
    cout << "What is your first name? ";
    cin.getline(first_name,20);
    cout << "What is your last name? ";
    cin.getline(last_name,20);
    cout << "What letter grade do you deserve? ";
    cin >> grade;
    cout << "What is your age? ";
    cin >> age;
    cout << "Name: " << last_name << ", " << first_name << endl;
    cout << "Grade: " << char(grade+1) << endl;
    cout << "Age: " << age;

    return 0;
}