// Напи шите программу, которая приглашает пользователя ввести три результата
// забега на 40 ярдов ( или 40 метров, если желаете) и затем отображает эти
// значения и их среднее. Для хранения данных применяйте объект array. ( Если
// объект array не доступен, воспользуйтесь встроенным массиво м . )

#include <iostream>
#include <conio.h>
#include <array>

using namespace std;

const int N = 3;

int main()
{
    array<double, N> res;
    double sum, average;

    for (int i(1); i<=N; i++){
        cout << "Enter result " << i << " athlete: ";
        cin >> res[i];
        sum+=res[i];
    }

    average=sum/N;
    cout << "Average: " << average;
    return 0;
}