// Перепишите листинг 4.4, применив класс С++ s t r i ng вместо массивов char.

#include <iostream>
#include <string>
using namespace std;

int main()
{
    string name, dessert;
    cout << "Enter your name: \n";
    getline(cin, name);
    cout << "Enter your favorite dessert: \n";
    getline(cin, dessert);
    cout << "I have some delicious " << dessert;
    cout << " for you, " << name << ".\n";

    return 0;
}