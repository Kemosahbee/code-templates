// Вильям Вингейт (William Wingate) заведует службой анализа рынка пиццы.
// О каждой пицце он записывает следующую информацию:
// • наименование компании - производителя пиццы , которое может состоять
// из более чем одного слова;
// • диаметр пиццы;
// • вес пиццы.
// • Разработайте структуру, которая может содержать всю эту информацию, и
// напишите программу, использующую структурную переменную этого типа.
// Программа должна запрашивать у пользователя каждый из перечисленных
// показателей и затем отображать введенную информацию . Применяйте c i n
// (или его методы) и c o u t .

// Выполните упражнение 7 , но с применением операции n e w для размещения
// структуры в свободном хранилище вместо объявления структурной переменной.
// Кроме того , сделайте так, чтобы программа сначала запрашивала диаметр
// пиццы , а потом - наименование компании.

#include <iostream>
#include <conio.h>
#include <cstring>
#include <string>

using namespace std;

struct Pizza_market
{
    char name[20];
    double diameter;
    double weight;
};

int main()
{   
    //4-7
    Pizza_market Dodo;

    cout << "Enter pizza name: ";
    cin.getline(Dodo.name, 20);
    cout << "Enter diameter: ";
    cin >> Dodo.diameter;
    cout << "Enter weight: ";
    cin >> Dodo.weight;

    system("cls");

    cout << "You enter follow data: " << endl;
    cout << "Name: " << Dodo.name << endl;
    cout << "Diameter: " << Dodo.diameter << endl;
    cout << "Weight: " << Dodo.weight;
    cout << endl;

    //4-8
    Pizza_market *Dominos = new Pizza_market;
    
    cout << "Enter diameter: ";
    (cin >> Dominos->diameter).get();
    cout << "Enter pizza name: ";
    cin.getline(Dominos->name, 20);
    cout << "Enter weight: ";
    cin >> Dominos->weight;

    cout << "You enter follow data: " << endl;
    cout << "Diameter: " << Dominos->diameter << endl;
    cout << "Name: " << Dominos->name << endl;
    cout << "Weight: " << Dodo.weight;
    delete Dominos;

    return 0;
}