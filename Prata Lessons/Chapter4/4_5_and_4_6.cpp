// Структура CandyBar содержит три члена. Первый из н и х хранит название коробки
// конфет. Второй - ее вес ( который может иметь дробную часть) , а третий
// - ч и сл о калорий ( целое значение) . Напишите программу, объявляющую
// эту структуру и создающую переменную типа CandyBar по имени sna ck , инициализируя
// ее члены значениями "Mocha Munch " , 2 . 3 и 3 5 0 , соответственно.
// Инициализация должна быть частью объявления sna ck. И, наконец, программа
// должна отобразить содержимое этой переменной.

// Структура CandyBar включает три члена, как описано в предыдущем упражнении.
// Напишите программу, которая создает массив из трех структур CandyBa r,
// инициализирует их значениями по вашему усмотрению и затем отображает содержимое
// каждой структуры

#include <iostream>
#include <cstring>
#include <string>

using namespace std;

struct CandyBar
{
    char name[20];
    double weight;
    int ccal;
};

int main()
{
    CandyBar snack =  { "Mocha Munch", 2.3, 350 };

    cout << "Candieees!!" << endl;
    cout << "Name: " << snack.name << endl;
    cout << "weight: " << snack.weight << " kg." << endl;
    cout << "Ccal: " << snack.ccal << " calories" << endl;

    CandyBar sweet[3];

    for (int i(0); i<3; i++){
        cout << "Enter name of sweet number " << i+1 << " : ";
        cin.getline(sweet[i].name, 20);
        cout << "Enter weight of sweet number " << i+1 << " : ";
        cin >> sweet[i].weight;
        cout << "Enter ccal's of sweet number " << i+1 << " : ";
        (cin >> sweet[i].ccal).get();
        cin.clear();
    }

    system("cls");

    for (int i(0); i<3; i++){
        cout << "Name of sweet number " << i+1 << " : " << sweet[i].name << endl;
        cout << "Weight of sweet number " << i+1 << " : " << sweet[i].weight << endl;
        cout << "Ccal's of sweet number " << i+1 << " : " << sweet[i].ccal << endl;
    }

    return 0;
}