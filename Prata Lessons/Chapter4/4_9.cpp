// Выполните упражнение 6 , но вместо объявления массива из трех структур
// CandyBar используйте операцию new для динамического размещения массива.

#include <iostream>
#include <conio.h>
#include <cstring>
#include <string>

using namespace std;

struct CandyBar
{
    char name[20];
    double weight;
    int ccal;
};

int main()
{
    int Number;
    cout << "Enter number of candies!: ";
    (cin >> Number).get();

    CandyBar *sweet = new CandyBar[Number];

    for (int i(1); i<=Number; i++){
        cout << "Enter name of sweet number " << i << " : ";
        cin.getline(sweet[i].name, 20);
        cout << "Enter weight of sweet number " << i << " : ";
        cin >> sweet[i].weight;
        cout << "Enter ccal's of sweet number " << i << " : ";
        (cin >> sweet[i].ccal).get();
        cin.clear();
    }

    delete [] sweet;

    return 0;
}