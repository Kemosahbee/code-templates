// Когда вы вступите в Благотворительный Орден Программистов (БОП) , к вам
// могут обращаться на заседаниях БОП по вашему реальному имени, по должности
// либо секретному имени БОП. Напишите программу, которая может выводить
// списки членов по реальным именам, должностям, секретным именам либо
// по предпочтению самого члена.

#include <iostream>
#include <cstring>

using namespace std;

const int strsize = 30;

struct bop
{
    char fullname[strsize];
    char title[strsize];
    char bopname[strsize];
    int preference; // 0=полное имя, 1=титул, 2=имя БОП
};

void showmenu()
{   
    cout << endl;
    cout << "a. display by name         b. display by title" << endl;
    cout << "c. display by bopname      d. display by preference" << endl;
    cout << "q. quit" << endl;
}

void showname();
void showtitle();
void showpreference();

int main()
{   

    bop proger[3];

    strcpy(proger[0].fullname, "Kostya Savkin");
    strcpy(proger[0].title, "junior developer");
    strcpy(proger[0].bopname, "Kemosahbee");
    proger[0].preference = 2;
    
    strcpy(proger[1].fullname, "Dmitry Shamanin");
    strcpy(proger[1].title, "middle developer");
    strcpy(proger[1].bopname, "Demohha");
    proger[1].preference = 1;

    strcpy(proger[2].fullname, "Anton Leshko");
    strcpy(proger[2].title, "senior developer");
    strcpy(proger[2].bopname, "Leshiy");
    proger[2].preference = 0;

    showmenu();
    cout << "Enter your choise: ";
    char choise;
    cin >> choise;
    while (choise != 'q')
    {
        switch (choise)
        {
            case 'a':
                for (int i(0); i<3; i++){
                    cout << proger[i].fullname << endl;
                }
                break;
            case 'b':
                for (int i(0); i<3; i++){
                    cout << proger[i].title << endl;
                }
                break;
            case 'c':
                for (int i(0); i<3; i++){
                    cout << proger[i].bopname << endl;
                }
                break;
            case 'd':
                for (int i(0); i<3; i++){
                    switch (proger[i].preference)
                    {
                        case 0:
                            cout << proger[i].fullname << endl;
                            break;
                        case 1:
                            cout << proger[i].title << endl;
                            break;
                        case 2:
                            cout << proger[i].bopname << endl;
                            break;
                    }
                }
                break;
            default:
                cout  << "You enter wrong choise. Please repeat: ";
                cin >> choise;
                continue;
        }
        cout << "Next choise: ";
        cin >> choise;

    }

    return 0;
}