// Выполните упражнение 6 , но измените его так , чтобы данные можно было получать
// из файла. Первым элементом файла должно быть количество меценатов,
// а остальная часть состоять· из пар строк , в которых первая строка содержит
// и мя , а вторая - сумму пожертвования .

#include <iostream>
#include <fstream>
using namespace std;

struct patrons
{
    char name[20];
    double val;
};

int main()
{
    ifstream in;
    in.open("6_9.txt");

    int number;
    in >> number;
    in.get();

    patrons *patr = new patrons[number];

    for (int i(0); i<number; i++){
        in.getline(patr[i].name, 20);
        in >> patr[i].val;
        in.get();
    }

    int count=0;
    cout << "~~~Grand patrons~~~" << endl;
    for (int i(0); i<number; i++){
        if (patr[i].val >=10000){
            cout << patr[i].name << "    " << patr[i].val << endl;
            count++;
        }
    }
    if (count==0){
        cout << "none" << endl;
    }

    count=0;
    cout << endl << "~~~Patrons~~~" << endl;
    for (int i(0); i<number; i++){
        if (patr[i].val <10000){
            cout << patr[i].name << "    " << patr[i].val << endl;
            count++;
        }
    }
    if (count==0){
        cout << "none" << endl;
    }
    
    delete [] patr;
    in.close();
    return 0;
}