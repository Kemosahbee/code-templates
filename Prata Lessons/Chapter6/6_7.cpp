// Напишите программу, которая читает слова по одному за раз, пока не будет введена
// отдельная буква q. После этого программа должна сообщить количество
// слов, начинающихся с гласных, количество слов, начинающихся с согласных,
// а также количество слов, не попадающих ни в одну из этих категорий. Одним
// из возможных подходов может быть применение i s a lpha ( ) для различения
// слов , начинающихся с букв, и остальных, с последующим применением if или
// s w i t ch для идентификации тех слов, прошедших проверку i s a l pha ( ) , которые
// начинаются с гласных.

#include <iostream>
#include <cstring>
#include <string>

using namespace std;

int main()
{
    char word[15];
    int vowels=0, consonants=0, others=0;
    cout << "Enter words (q to quit):" << endl;
    do
    {
        cin >> word;
        if (isalpha(word[0])){
            switch (word[0])
            {
                case 'a':
                case 'e':
                case 'y':
                case 'o':
                case 'u':
                case 'A':
                case 'E':
                case 'Y':
                case 'O':
                case 'U':
                    vowels++;
                    break;
                default:
                    consonants++;
                    break;
            }
        }else{
            others++;
        }
    } while (strcmp(word, "q")!=0);
    consonants--;
    cout << vowels << " words beginning with vowels" << endl;
    cout << consonants << " words beginning with consonants" << endl;
    cout << others << " others";
    return 0;
}