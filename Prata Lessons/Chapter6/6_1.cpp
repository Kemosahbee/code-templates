//Напишите программу, которая читает клавиатурный ввод до символа @
//и повторяет его, за исключением десятичных цифр, преобразуя каждую
//букву верхнего регистра в букву нижнего регистра и наоборот.

#include <iostream>
#include <cctype>

using namespace  std;

int main()
{   
    char ch;
    cout << "Enter your text: ";
    cin.get(ch);
    while (ch!='@')
    {
        if (islower(ch))
        {
            cout << (char)toupper(ch);
        }
        else if (isupper(ch))
        {
            cout << (char)tolower(ch);
        }
        else if (isdigit(ch))
        {
            
        }
        else
        {
            cout << ch;
        }
        cin.get(ch);
    }

    return 0;
}