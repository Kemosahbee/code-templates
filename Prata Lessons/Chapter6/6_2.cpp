//Напишите программу, читающую в массив douЫe до 10 значений пожертвований.
//( Или , если хотите , используйте шаблонный объект array. ) Программа
//должна прекращать ввод при получении нечисловой величины. Она должна
//выдавать среднее значение полученных чисел , а также количество значений в
//массиве, превышающих среднее.

#include <iostream>

using namespace std;

const int ArSize = 10;

int main()
{
    double donation[ArSize];
    int i = 0;

    cout << "Donation number 1: ";
    while (i < ArSize && cin >> donation[i])
    {
        if (++i < ArSize)
        {
            cout << "Donation number " << i+1 << ": ";
        }
    }

    double total=0.0;
    int count=0;

    if (i==0){
        cout << "No donations" << endl;
    }else{
        for (int j(0); j<i; j++){
            total+=donation[j];
        }
        cout << "Total donations: "<< total << endl;
//////////////////////////////////////////////////////////
        cout << total/i << " average donation" << endl;
//////////////////////////////////////////////////////////
        for (int j(0); j<i; j++){
            if (donation[j] > total/i){
                count++;
            }
        }
        cout << "Values more then average: " << count;
    }



    return 0;
}