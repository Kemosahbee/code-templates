// Напишите предшественник программы , управляемой меню. Она должна
// отображать меню из четырех пунктов , каждый из них помечен буквой. Если
// пользователь вводит букву, отличающуюся от четырех допустимых, программа
// должна повторно приглашать его ввести правильное значение до тех пор, пока
// он этого не сделает. Затем она должна выполнить некоторое простое действие
// на основе пользовательского выбора.

#include <iostream>

using namespace std;

void showmenu()
{   
    cout << endl;
    cout << "Please enter one of the following choises:" << endl;
    cout << "c) carnivore   p) pianist" << endl;
    cout << "t) tree        g) game" << endl;
}


int main()
{
    char ch;
    showmenu();
    cin >> ch;

    while (1)
    {
        switch (ch)
        {
        case 'c':
            cout << "You enter 'c' = carnivore" << endl;
            break;
        case 'p':
            cout << "You enter 'p' = pianist" << endl;
            break;
        case 't':
            cout << "You enter 't' = tree" << endl;
            break;
        case 'g':
            cout << "You enter 'g' = game" << endl;
            break;
        default:
            cout << "Please enter a c, p, t, or g: " << endl;
            cin >> ch;
            continue;
        }
        break;
    }
    return 0;
}