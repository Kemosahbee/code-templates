// Постройте программу, которая отслеживает пожертвования в Общество
// Защиты Влиятельных Лиц. Она должна запрашивать у пользователя количество
// меценатов , а затем приглашать вводить их имена и суммы пожертвований от
// каждого. Информация должна сохраняться в динамически выделяемом массиве
// структур. Каждая структура должна иметь два члена: символьный массив ( или
// объект s t r i n g ) для хранения имени и переменную-член типа douЫ e - для
// хранения суммы пожертвования. После чтения всех данных программа должна
// отображать имена и суммы пожертвований тех, кто не пожалел $ 1 0 ООО и более.
// Этот список должен быть озаглавлен меткой "Graпd Patroпs". После этого
// программа должна выдать список остальных жертвователей. Он должен быть
// озаглавлен "Patroпs". Если в одной из двух категорий не окажется никого , программа
// должна напечатать "попе". Помимо отображения двух категорий, никакой
// другой сортировки делать не нужно.

#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

struct patrons
{
    string name;
    double total;
};

int main()
{
    int number;
    cout << "Enter number of patrons: ";
    while (!(cin >> number))
    {   
        cin.clear();
        while (cin.get()!='\n'){
            continue;
        }
        cout << "Please enter proper number: ";
    }
    cin.get();

    patrons *patr = new patrons[number];

    for (int i(0); i<number; i++){
        cout << "Enter name " << i+1 << " patron: ";
        getline(cin,patr[i].name);
        cout << "Enter sum " << i+1 << " patron: ";
        cin >> patr[i].total;
        cin.get();
    }

    system("cls");

    int count=0;
    cout << "~~~Grand Patrons~~~" << endl;
    for (int i(0); i<number; i++){
        if (patr[i].total >= 10000){
            cout << patr[i].name << "   " << patr[i].total << "$" << endl;
            count++;
        }
    }
    if (count==0){
        cout << "none" << endl;
    }
    cout << "____________________" << endl;

    count=0;
    cout << endl << "~~~Patrons~~~" << endl;
    for (int i(0); i<number; i++){
        if (patr[i].total < 10000){
            cout << patr[i].name << "   " << patr[i].total << "$" << endl;
            count++;
        }
    }
    if (count==0){
        cout << "none" << endl;
    }
    cout << "____________________" << endl;
    
    delete [] patr;

    return 0;
}