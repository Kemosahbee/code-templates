// Напишите программу, которая открывает текстовый файл , читает его символ
// за символом до самого конца и сообщает количество символов в файле.

#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

int main()
{
    ifstream inFile;
    inFile.open("6_8.txt");

    if(!inFile.is_open()){
        cout << "Could not open file" << endl;
        cout << "Program terminating" << endl;
        exit(EXIT_FAILURE);
    }

    char ch;
    int count=0;
    inFile.get(ch);
    while (inFile.good())
    {
        count++;
        if (ch=='\n'){
            count--;
        }
        inFile.get(ch);
    }
    cout << count << " characters in file";
    inFile.close();
    return 0;
}