// Королевство Нейтрония, где денежной единицей служит тварп, использует следующую
// шкалу налогообложения:
// первые 5 ООО тварпов - налог 0%
// следующие 10 ООО тварпов - налог 10%
// следующие 20 ООО тварпов - налог 15%
// свыше 35 О ОО тварпов - налог 20%
// Например, если некто зарабатывает 38 ООО тварпов, то он должен заплатить налогов
// 5000 х 0,00 + 1 0000 х 0 , 1 0 + 20000 х 0, 1 5 + 3000 х 0,20, или 4 600 тварпов.
// Напишите программу, которая использует цикл для запроса доходов и выдачи
// подлежащего к выплате налога. Цикл должен завершаться, когда пользователь
// вводит отрицательное или нечисловое значение.

#include <iostream>

using namespace std;

int main()
{   
    int income;
    double tax=0.0;
    double taxRate5=0.0, taxRate10=0.1, taxRate20=0.15, taxRate35=0.2;
    cout << "Enter your income: ";
    while (cin>>income && income >=0)
    {
        if (income <= 5000){
            tax=income*taxRate5;
        }else if (income > 5000 && income<=15000){
            tax=5000*taxRate5+(income-5000)*taxRate10;
        }else if (income > 15000 && income<=35000){
            tax=5000*taxRate5+10000*taxRate10+(income-15000)*taxRate20;
        }else if (income>35000){
             tax=5000*taxRate5+10000*taxRate10+20000*taxRate20+(income-35000)*taxRate35;
        }
        cout << tax << endl;
        cout << "Enter your income: ";
    }
    
    return 0;
}