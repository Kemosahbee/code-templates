#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

#define _D(lvl, ...) if (lvl <= stack_log_level) {printf(__VA_ARGS__);}
static int stack_log_level;
void stack_set_log_level(int new_level)
{
    stack_log_level=new_level;
}

static size_t total_push;
static size_t total_pop;
static size_t total_alloc;
static size_t total_free;
static size_t total_head_alloc_fail;
static size_t total_data_alloc_fail;

void statistic_output(){
    printf("Total push: %d\n", total_push);
    total_push=0;
    printf("Total pop: %d\n", total_pop);
    total_pop=0;
    printf("Total alloc: %d\n", total_alloc);
    total_alloc=0;
    printf("Total free: %d\n", total_free);
    total_free=0;
    printf("Total head alloc fail: %d\n", total_head_alloc_fail);
    total_head_alloc_fail=0;
    printf("Total data alloc fail: %d\n", total_data_alloc_fail);
    total_data_alloc_fail=0;
}

int   stack_push(stack_t **stk, const void *data, size_t size)
{
    if (!stk || !data || !size)
    {
        _D(STK_D_WRN, "stack_push:: wrong arg: stk=%p, data=%p, size=%lu\n", stk, data, size);
        return STK_WRONG_ARG;
    }
      
    stack_t *head = (stack_t*)malloc(sizeof(stack_t));
    if (!head)
    {
        _D(STK_D_ERR, "stack_push:: failed to alloc stack head\n");
        total_head_alloc_fail++;
        return STK_MALLOC_FAILED;
    }
    total_alloc+=sizeof(stack_t);

    head->data=malloc(size);
    if (head->data==NULL)
    {
        free(head);
        total_head_alloc_fail++;
        total_free+=sizeof(stack_t);
        _D(STK_D_ERR, "stack_push:: failed to alloc data head\n");
        return STK_MALLOC_FAILED;
    }
    total_alloc+=size;
    
    memcpy(head->data, data, size);
    head->prev=*stk;
    *stk=head;
    head->size=size;
    total_push++;
    return STK_OK;
}

int   stack_top (const stack_t *stk, void *data, size_t size)
{
    if (!stk || !data)
    {
        _D(STK_D_WRN, "stack_top:: wrong arg: stk=%p, data=%p, size=%lu\n", stk, data, size);
        return STK_WRONG_ARG;
    }
    if (stk->size > size)
    {
        _D(STK_D_ERR, "stack_top:: not enough data\n");
        return STK_NOT_ENOUGH_DATA;
    }

    memcpy(data, stk->data, stk->size);
    return STK_OK;
}

int   stack_pop (stack_t **stk, void *data, size_t size)
{
    if (!stk)
    {
        _D(STK_D_WRN, "stack_pop:: wrong arg: stk=%p, data=%p, size=%lu\n", stk, data, size);
        return STK_WRONG_ARG;  
    }

    int result = stack_top(*stk, data, size);
    if (result==0){
        free((*stk)->data);
        total_free+=(*stk)->size;
        stack_t *tmp=*stk;
        *stk=(*stk)->prev;
        free(tmp);
        total_free+=sizeof(stack_t);
        total_pop++;
    }
    return result;
}

size_t stack_size (const stack_t *stk) 
{
    size_t  result=0;
    while(stk){
        result++;
        stk=stk->prev;
    }
    return result;
}
