#include <iostream>
#include <string.h>

void reverse(const char *s){
    int length=strlen(s);
    char rephrase;
    char *s1=new char [length+1];
    strcpy(s1,s);
    
    //Полностью меняем порядок букв в строке
    for (int i=0; i<length/2; i++ ){
        rephrase=s1[i];
        s1[i]=s1[length-i-1];
        s1[length-i-1]=rephrase;
    }
    //Находим начало и конец слова и "отзеркаливаем" его
    int j=0;
    for (int i = 0; i<=length; i++){
        if (s1[i]=='.' || s[i]=='\0'){
            for (int k=j; k<(i+j)/2; k++ ){
                rephrase=s1[k];
                s1[k]=s1[i+j-k-1];
                s1[i+j-k-1]=rephrase;
            }
            j=i+1;
        }
    }
    printf("%s\n", s1);
}

int main() {
    reverse("hello.world");      // world.hello
    reverse("where.is.my.mind"); // mind.my.is.where
    return 0;
}