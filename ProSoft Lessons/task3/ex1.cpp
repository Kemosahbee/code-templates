#include <iostream>
#include <string.h>
#include <stdlib.h>

void round10(const char *n){
    int length=0;
    int i=0;
    length=strlen(n);
    char *n1 = new char [length] ;
    char *n2 = new char [0] ;
    n1=strcpy(n1, n);
    strcpy(n2, "1");

    if (n1[length-1]<'5'){
        n1[length-1]='0';
    }else{
        n1[length-1]='0';
        while (n1[length-2-i]=='9'){
            n1[length-2-i]='0';
            i++;
        }
        n1[length-2-i]=n1[length-2-i]+1;    
    }

    //Увеличение числа на разряд
    if ((i==length-1) && (n[length-1]>'5')){
        strcat(n2,n1);
        printf("%s\n",n2);
        return;
    }

    printf("%s\n",n1);
}

int main() {
    round10("1234");  // 1230
    round10("199");   // 200
    round10("999");   // 1000
    round10("9");     // 10
    round10("0");     // 0
    round10("6");     // 10
    return 0;
}
