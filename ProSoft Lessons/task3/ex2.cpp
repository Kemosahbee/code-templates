#include <iostream>
#include <string.h>
using namespace std;

void add_bin(const char *s1, const char *s2){
    int length1=strlen(s1);
    int length2=strlen(s2);
    int length;
    int num1;
    int num2;
    int rank=0;
    char sum;
    string bin1=s1;
    string bin2=s2;
    string bin;

    // Заполнение старших разрядов нулями
    if (length1 > length2){
        for (int i=length2; i<length1; ++i){
            bin2='0'+bin2;
        }
        length=length1;
    }else{
        for (int i=length1; i<length2; ++i){
            bin1='0'+bin1;
        }
        length=length2;
    }

    for (int i = length-1; i>=0; --i){
        num1=bin1[i]-'0';
        num2=bin2[i]-'0';
        sum=(num1^num2^rank)+'0';
        bin=sum+bin;
        if ((num1==1 && num2==1) || (num1==1 && rank==1) || (num2==1 && rank==1)){
            rank=1;
        }else{
            rank=0;
        }
    }

    if (rank==1){
        bin='1'+bin;
    }

    cout<<bin<<endl;
    return;
}

int main() {
    add_bin("1",   "10");// 11
    add_bin("1",   "1"); // 10
    add_bin("1111","1"); // 10000
    // Ваши тесты...
    return 0;
}
