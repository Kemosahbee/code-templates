#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

int   stack_push(stack_t **stk, const void *data, size_t size)
{
    if (!stk || !data || !size)
        return STK_WRONG_ARG;
      
    stack_t *head = (stack_t*)malloc(sizeof(stack_t));
    if (head==NULL){
        return STK_MALLOC_FAILED;
    }
    head->data=malloc(size);
    if (head->data==NULL){
        free(head);
        return STK_MALLOC_FAILED;
    }
    memcpy(head->data, data, size);
    head->prev=*stk;
    *stk=head;
    head->size=size;

   // TODO: malloc and copy for data; update stack head; check errors!
    return STK_OK;
}

int   stack_top (const stack_t *stk, void *data, size_t size)
{
    if (!stk || !data)
        return STK_WRONG_ARG;
      
    if (stk->size > size)
        return STK_NOT_ENOUGH_DATA;
      
    memcpy(data, stk->data, stk->size);
    return STK_OK;
}

int   stack_pop (stack_t **stk, void *data, size_t size)
{
    if (!stk)
        return STK_WRONG_ARG;  
  
    int result = stack_top(*stk, data, size);
    if (result==0){
        memcpy(data, (*stk)->data, size);
        free((*stk)->data);
        stack_t *tmp=*stk;
        *stk=(*stk)->prev;
        free(tmp);
    }
   // TODO: if OK - update *stk and free memory here
    return result;
}

size_t stack_size (const stack_t *stk) 
{  // TODO: count size here
    size_t  result=0;
    while(stk){
        result++;
        stk=stk->prev;
    }
    return result;
}
