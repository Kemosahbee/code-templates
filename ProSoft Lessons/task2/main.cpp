#include <stdio.h>
 
int prosoft_atoi(const char *str){
	
	if (str==NULL){
		return 0;
	}
	
	int val=0;
	int sign=1;
	int k=0;

	// Игнорирование пробелов
	while (str[k]==' '){
		k++;
	}
	
	// Проверка знака
	if (str[k]=='-'){
		sign=-1;
		k++;
	}
	
	for (int i=k; str[i] ; i++){
		if (str[i]>='0' && str[i]<='9'){
			val=val*10+(str[i]-'0');
		}else{
			return val*sign;
		}
	}
	return val*sign;
}

int main() {
    printf("Test1: %d\n", prosoft_atoi("1"));         // 1
    printf("Test2: %d\n", prosoft_atoi("-1"));        // -1
    printf("Test3: %d\n", prosoft_atoi("1234567890"));// 1234567890
    printf("Test4: %d\n", prosoft_atoi(""));          // 0
    printf("Test5: %d\n", prosoft_atoi("test"));      // 0
    printf("Test6: %d\n", prosoft_atoi("22test"));    // 22
    printf("Test7: %d\n", prosoft_atoi(NULL));        // 0
    return 0;
}