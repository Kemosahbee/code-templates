#include "client.h"
#include "ui_client.h"
#include <QtWidgets>

Client::Client(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::Client)
{
    ui->setupUi(this);
    ui->pushButton_2->setDefault(true);
    ui->pushButton_2->setDisabled(true);
    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));
    connect(ui->lineEdit,SIGNAL(textChanged(QString)),this,SLOT(on_Text_Changed(const QString)));
    connect(ui->lineEdit,SIGNAL(returnPressed()),this,SLOT(on_pushButton_2_clicked()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(connection_Lost()));
}

Client::~Client()
{
    delete ui;
}

void Client::on_pushButton_clicked()
{
    socket->connectToHost("127.0.0.1",12345);
}

void Client::sockDisc()
{
    socket->deleteLater();
}

void Client::sockReady()
{
    QString ip = socket->peerName();
    quint16 port = socket->peerPort();
    ui->lineEdit_2->setText(ip);
    ui->lineEdit_3->setText(QString::number(port));
    QString user = socket->peerName();
    while(socket->canReadLine())
    {
        QString message = QString::fromUtf8(socket->readLine().trimmed());
        ui->textEdit->append(user + ": " + message);
        qDebug()<<message;
    }
}

void Client::on_pushButton_2_clicked()
{
    QString message = ui->lineEdit->text().trimmed();
    socket->write((message+"\n").toUtf8());
    ui->lineEdit->clear();
}

void Client::on_Text_Changed(const QString str)
{
    ui->pushButton_2->setEnabled(!str.isEmpty());
}

void Client::connection_Lost()
{
    ui->textEdit->append("Connection lost");
}