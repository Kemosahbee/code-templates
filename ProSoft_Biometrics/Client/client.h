#ifndef CLIENT_H
#define CLIENT_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QMessageBox>
#include <QDebug>
#include <QTextBrowser>

namespace Ui {
class Client;
}

class Client : public QMainWindow
{
    Q_OBJECT

    public:
        explicit Client(QWidget *parent = 0);
        ~Client();
    private:
        QTcpSocket* socket;

    public slots:
        void sockReady();
        void sockDisc();

    private slots:
        void on_pushButton_clicked();
        void on_Text_Changed(const QString str);
        void on_pushButton_2_clicked();
        void connection_Lost();

private:
        Ui::Client *ui;
};

#endif