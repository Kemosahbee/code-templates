#include "myserver.h"

myserver::myserver(QObject *parent) : QTcpServer(parent){}

myserver::~myserver(){}

void myserver::startServer()
{
    if (this->listen(QHostAddress::Any,12345))
    {
        qDebug()<<"Waiting for connections";
    }
    else
    {
        qDebug()<<"Not listening";
    }
}

void myserver::incomingConnection(qintptr handle)
{

    QTcpSocket *socket = new QTcpSocket(this);
    socket->setSocketDescriptor(handle);
    sockets.insert(socket);
    connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));

    qDebug()<< handle << "Client connected";

    socket->write("You are connect\n");
//    qDebug()<<"Send client connect status - YES";
}

void myserver::sockReady()
{
    QTcpSocket *socket = (QTcpSocket*)sender();
    QString user = socket->peerName();
    while(socket->canReadLine())
    {
        QByteArray message = socket->readLine().trimmed();
        foreach(QTcpSocket *otherSocket, sockets)
        {
//            otherSocket->write((user + ": ").toUtf8());
            otherSocket->write(message + "\n");
        }
    }
}

void myserver::sockDisc()
{
    QTcpSocket *socket = (QTcpSocket*)sender();
    QHostAddress user = socket->peerAddress();
    QHostAddress ipv4(user.toIPv4Address());
    foreach(QTcpSocket *socket, sockets)
        socket->write(QString("Server:" + ipv4.toString() + " has left.\n").toUtf8());
    sockets.remove(socket);
    socket->deleteLater();
}