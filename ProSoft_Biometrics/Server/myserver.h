#ifndef MYSERVER_H
#define MYSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QSet>

class myserver: public QTcpServer
{
    Q_OBJECT
    public:
        myserver(QObject *parent=0);
        ~myserver();
    private:
        QSet<QTcpSocket*> sockets;

    public slots:
        void startServer();
        void incomingConnection(qintptr handle);
        void sockReady();
        void sockDisc();
};

#endif // MYSERVER_H