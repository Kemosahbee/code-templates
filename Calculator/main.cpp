#include "calculator.h"
#include <QApplication>
#include <QDebug>
#include <QtSql>

int main(int argc, char *argv[])
{

    // Открытие базы данных, либо создание если ее еще не существует
    QSqlDatabase historyDB = QSqlDatabase::addDatabase("QSQLITE");
    historyDB.setDatabaseName("history_database.sqlite");
    if (!historyDB.open()) {
        qDebug() << historyDB.lastError().text();
    }

    // Создание таблицы в базе
      QString str  = "CREATE TABLE IF NOT EXISTS HISTORY ( "
                         "[date]                TEXT, "
                         "[first operator]      TEXT, "
                         "[operand]             TEXT, "
                         "[second operator]     TEXT, "
                         "[result]              TEXT"
                     ");";
    QSqlQuery query;
    if (!query.exec(str)) {
        qDebug() << "Unable to create a table";
    }

    QApplication a(argc, argv);
    Calculator w;
    w.setFixedSize(305,387);
    w.show();
    return a.exec();
}
