#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QMainWindow>

namespace Ui {
class Calculator;
}

class Calculator : public QMainWindow
{
    Q_OBJECT

public:
    explicit Calculator(QWidget *parent = nullptr);
    ~Calculator();

public slots:

private slots:

    void on_PB_0_clicked();

    void on_PB_1_clicked();

    void on_PB_2_clicked();

    void on_PB_3_clicked();

    void on_PB_4_clicked();

    void on_PB_5_clicked();

    void on_PB_6_clicked();

    void on_PB_7_clicked();

    void on_PB_8_clicked();

    void on_PB_9_clicked();

    void on_PB_dot_clicked();

    void on_PB_changeSign_clicked();

    void on_PB_plus_clicked();

    void on_PB_minus_clicked();

    void on_PB_mult_clicked();

    void on_PB_divide_clicked();

    void on_PB_clearAll_clicked();

    void on_PB_equal_clicked();

    void on_PB_percent_clicked();

    void on_PB_backspace_clicked();

    void on_PB_sqrt_clicked();

    void on_PB_MS_clicked();

    void on_PB_MC_clicked();

    void on_PB_MR_clicked();

    void keyPressEvent(QKeyEvent *event);
private:
    Ui::Calculator *ui;
    QString firstOperator="0";
    QString secondOperator="0";
    double memory; // пямять
    char op; // операнд
    bool resultFlag; // на дисплей выведен результат
    bool firstOperatorFlag=true; // на дисрлее первый оператор
    bool dotFlag; // в операторе присутствует дробная часть
    bool waitForResult; // ждет нажатия кнопки "="(включается, когда появляется второй оператор)
    bool error; // ошибка (попытка взять корень из отрицательного числа)
    bool memoryReadFlag;
    bool memorySaveFlag;
};

#endif // CALCULATOR_H
