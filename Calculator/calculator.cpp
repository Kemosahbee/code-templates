#include "calculator.h"
#include "ui_calculator.h"
#include <QtMath>
#include <QDebug>
#include <QtSql>
#include <QDateTime>
#include <QKeyEvent>

Calculator::Calculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Calculator)
{
    ui->setupUi(this);
    ui->memSign->setVisible(false);
    on_PB_clearAll_clicked();
}

Calculator::~Calculator()
{
    delete ui;  
}

void Calculator::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
        case Qt::Key_0:
            on_PB_0_clicked();
            break;
        case Qt::Key_1:
            on_PB_1_clicked();
            break;
        case Qt::Key_2:
            on_PB_2_clicked();
            break;
        case Qt::Key_3:
            on_PB_3_clicked();
            break;
        case Qt::Key_4:
            on_PB_4_clicked();
            break;
        case Qt::Key_5:
            on_PB_5_clicked();
            break;
        case Qt::Key_6:
            on_PB_6_clicked();
            break;
        case Qt::Key_7:
            on_PB_7_clicked();
            break;
        case Qt::Key_8:
            on_PB_8_clicked();
            break;
        case Qt::Key_9:
            on_PB_9_clicked();
            break;
        case Qt::Key_Backspace:
            on_PB_backspace_clicked();
            break;
        case Qt::Key_Plus:
            on_PB_plus_clicked();
            break;
        case Qt::Key_Minus:
            on_PB_minus_clicked();
            break;
        case Qt::Key_Asterisk:
            on_PB_mult_clicked();
            break;
        case Qt::Key_Slash:
            on_PB_divide_clicked();
            break;
        case Qt::Key_Equal:
            on_PB_equal_clicked();
            break;
        case Qt::Key_Return:
            on_PB_equal_clicked();
            break;
        case Qt::Key_Period:
            on_PB_dot_clicked();
            break;
        case Qt::Key_Comma:
            on_PB_dot_clicked();
            break;
        case Qt::Key_Delete:
            on_PB_clearAll_clicked();
            break;
        case Qt::Key_Percent:
            on_PB_percent_clicked();
            break;
    }
}

void Calculator::on_PB_0_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="0";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (firstOperatorFlag){
                if (memoryReadFlag){
                    firstOperator="0";
                    ui->display->setText(firstOperator);
                    memoryReadFlag=false;
                }else{
                    if (firstOperator!="0"){
                        firstOperator+="0";
                    }
                }
                ui->display->setText(firstOperator);
            }else{
                if (memoryReadFlag){
                    secondOperator="0";
                    ui->display->setText(firstOperator+op+secondOperator);
                    memoryReadFlag=false;
                }else{
                    if (secondOperator!="0"){
                        secondOperator+="0";
                    }
                }
                waitForResult=true;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_1_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="1";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (firstOperatorFlag){
                if (memoryReadFlag){
                    firstOperator="1";
                    ui->display->setText(firstOperator);
                    memoryReadFlag=false;
                }else{
                    if (firstOperator=="0"){
                        firstOperator="1";
                    }else{
                        firstOperator+="1";
                    }
                }
                ui->display->setText(firstOperator);
            }else{
                if (memoryReadFlag){
                    secondOperator="1";
                    ui->display->setText(firstOperator+op+secondOperator);
                    memoryReadFlag=false;
                }else{
                    if (secondOperator=="0"){
                        secondOperator="1";
                    }else{
                        secondOperator+="1";
                    }
                }
                waitForResult=true;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_2_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="2";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (firstOperatorFlag){
                if (memoryReadFlag){
                    firstOperator="2";
                    ui->display->setText(firstOperator);
                    memoryReadFlag=false;
                }else{
                    if (firstOperator=="0"){
                        firstOperator="2";
                    }else{
                        firstOperator+="2";
                    }
                }
                ui->display->setText(firstOperator);
            }else{
                if (memoryReadFlag){
                    secondOperator="2";
                    ui->display->setText(firstOperator+op+secondOperator);
                    memoryReadFlag=false;
                }else{
                    if (secondOperator=="0"){
                        secondOperator="2";
                    }else{
                        secondOperator+="2";
                    }
                }
                waitForResult=true;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_3_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="3";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (firstOperatorFlag){
                if (memoryReadFlag){
                    firstOperator="3";
                    ui->display->setText(firstOperator);
                    memoryReadFlag=false;
                }else{
                    if (firstOperator=="0"){
                        firstOperator="3";
                    }else{
                        firstOperator+="3";
                    }
                }
                ui->display->setText(firstOperator);
            }else{
                if (memoryReadFlag){
                    secondOperator="3";
                    ui->display->setText(firstOperator+op+secondOperator);
                    memoryReadFlag=false;
                }else{
                    if (secondOperator=="0"){
                        secondOperator="3";
                    }else{
                        secondOperator+="3";
                    }
                }
                waitForResult=true;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_4_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="4";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (firstOperatorFlag){
                if (memoryReadFlag){
                    firstOperator="4";
                    ui->display->setText(firstOperator);
                    memoryReadFlag=false;
                }else{
                    if (firstOperator=="0"){
                        firstOperator="4";
                    }else{
                        firstOperator+="4";
                    }
                }
                ui->display->setText(firstOperator);
            }else{
                if (memoryReadFlag){
                    secondOperator="4";
                    ui->display->setText(firstOperator+op+secondOperator);
                    memoryReadFlag=false;
                }else{
                    if (secondOperator=="0"){
                        secondOperator="4";
                    }else{
                        secondOperator+="4";
                    }
                }
                waitForResult=true;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_5_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="5";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (firstOperatorFlag){
                if (memoryReadFlag){
                    firstOperator="5";
                    ui->display->setText(firstOperator);
                    memoryReadFlag=false;
                }else{
                    if (firstOperator=="0"){
                        firstOperator="5";
                    }else{
                        firstOperator+="5";
                    }
                }
                ui->display->setText(firstOperator);
            }else{
                if (memoryReadFlag){
                    secondOperator="5";
                    ui->display->setText(firstOperator+op+secondOperator);
                    memoryReadFlag=false;
                }else{
                    if (secondOperator=="0"){
                        secondOperator="5";
                    }else{
                        secondOperator+="5";
                    }
                }
                waitForResult=true;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_6_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="6";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (firstOperatorFlag){
                if (memoryReadFlag){
                    firstOperator="6";
                    ui->display->setText(firstOperator);
                    memoryReadFlag=false;
                }else{
                    if (firstOperator=="0"){
                        firstOperator="6";
                    }else{
                        firstOperator+="6";
                    }
                }
                ui->display->setText(firstOperator);
            }else{
                if (memoryReadFlag){
                    secondOperator="6";
                    ui->display->setText(firstOperator+op+secondOperator);
                    memoryReadFlag=false;
                }else{
                    if (secondOperator=="0"){
                        secondOperator="6";
                    }else{
                        secondOperator+="6";
                    }
                }
                waitForResult=true;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_7_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="7";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (firstOperatorFlag){
                if (memoryReadFlag){
                    firstOperator="7";
                    ui->display->setText(firstOperator);
                    memoryReadFlag=false;
                }else{
                    if (firstOperator=="0"){
                        firstOperator="7";
                    }else{
                        firstOperator+="7";
                    }
                }
                ui->display->setText(firstOperator);
            }else{
                if (memoryReadFlag){
                    secondOperator="7";
                    ui->display->setText(firstOperator+op+secondOperator);
                    memoryReadFlag=false;
                }else{
                    if (secondOperator=="0"){
                        secondOperator="7";
                    }else{
                        secondOperator+="7";
                    }
                }
                waitForResult=true;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_8_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="8";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (firstOperatorFlag){
                if (memoryReadFlag){
                    firstOperator="8";
                    ui->display->setText(firstOperator);
                    memoryReadFlag=false;
                }else{
                    if (firstOperator=="0"){
                        firstOperator="8";
                    }else{
                        firstOperator+="8";
                    }
                }
                ui->display->setText(firstOperator);
            }else{
                if (memoryReadFlag){
                    secondOperator="8";
                    ui->display->setText(firstOperator+op+secondOperator);
                    memoryReadFlag=false;
                }else{
                    if (secondOperator=="0"){
                        secondOperator="8";
                    }else{
                        secondOperator+="8";
                    }
                }
                waitForResult=true;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_9_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="9";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (firstOperatorFlag){
                if (memoryReadFlag){
                    firstOperator="9";
                    ui->display->setText(firstOperator);
                    memoryReadFlag=false;
                }else{
                    if (firstOperator=="0"){
                        firstOperator="9";
                    }else{
                        firstOperator+="9";
                    }
                }
                ui->display->setText(firstOperator);
            }else{
                if (memoryReadFlag){
                    secondOperator="9";
                    ui->display->setText(firstOperator+op+secondOperator);
                    memoryReadFlag=false;
                }else{
                    if (secondOperator=="0"){
                        secondOperator="9";
                    }else{
                        secondOperator+="9";
                    }
                }
                waitForResult=true;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_dot_clicked()
{
    if (!error){
        if (resultFlag){
            firstOperator="0.";
            ui->display->setText(firstOperator);
            resultFlag=false;
        }else{
            if (!dotFlag){
                if (firstOperatorFlag){
                    if (memoryReadFlag){
                        firstOperator="0.";
                        ui->display->setText(firstOperator);
                        memoryReadFlag=false;
                    }else{
                        if (firstOperator==""){
                            firstOperator="0.";
                        }else{
                            firstOperator+=".";
                        }
                    }
                    ui->display->setText(firstOperator);
                }else{
                    if (memoryReadFlag){
                        secondOperator="0.";
                        ui->display->setText(firstOperator);
                        waitForResult=true;
                        memoryReadFlag=false;
                    }else{
                        if (secondOperator==""){
                            secondOperator="0.";
                            waitForResult=true;
                        }else{
                            secondOperator+=".";
                        }
                    }
                    ui->display->setText(firstOperator+op+secondOperator);
                }
            }
            dotFlag=true;
        }
    }
}

void Calculator::on_PB_changeSign_clicked()
{
    if (!error){
        if (firstOperatorFlag){
            if (firstOperator.toDouble()>0.0){
                firstOperator='-'+firstOperator;
                ui->display->setText(firstOperator);
            }else if (firstOperator.toDouble()<0.0){
                firstOperator.remove('-');
                ui->display->setText(firstOperator);
            }
        }else{
            if (secondOperator.toDouble()>0.0){
                secondOperator='-'+secondOperator;
                ui->display->setText(firstOperator+op+secondOperator);
            }else if (secondOperator.toDouble()<0.0){
                secondOperator.remove('-');
                ui->display->setText(firstOperator+op+secondOperator);
            }

        }
    }
}

void Calculator::on_PB_plus_clicked()
{
    if (!error){
        if (!waitForResult){
            if (firstOperator.back()=='.'){
                firstOperator.chop(1);
            }
            op='+';
            ui->display->setText(firstOperator+op);
            firstOperatorFlag=false;
            dotFlag=false;
            resultFlag=false;
            memoryReadFlag=false;
            secondOperator="0";
        }
    }
}

void Calculator::on_PB_minus_clicked()
{
    if (!error){
        if (!waitForResult){
            if (firstOperator.back()=='.'){
                firstOperator.chop(1);
            }
            op='-';
            ui->display->setText(firstOperator+op);
            firstOperatorFlag=false;
            dotFlag=false;
            resultFlag=false;
            memoryReadFlag=false;
            secondOperator="0";
        }
    }
}

void Calculator::on_PB_mult_clicked()
{
    if (!error){
        if (!waitForResult){
            if (firstOperator.back()=='.'){
                firstOperator.chop(1);
            }
            op='x';
            ui->display->setText(firstOperator+op);
            firstOperatorFlag=false;
            dotFlag=false;
            resultFlag=false;
            memoryReadFlag=false;
            secondOperator="0";
        }
    }
}

void Calculator::on_PB_divide_clicked()
{
    if (!error){
        if (!waitForResult){
            if (firstOperator.back()=='.'){
                firstOperator.chop(1);
            }
            op='/';
            ui->display->setText(firstOperator+op);
            firstOperatorFlag=false;
            dotFlag=false;
            resultFlag=false;
            memoryReadFlag=false;
            secondOperator="0";
        }
    }
}

void Calculator::on_PB_clearAll_clicked()
{
    firstOperator="0";
    secondOperator="0";
    firstOperatorFlag=true;
    dotFlag=false;
    resultFlag=false;
    memoryReadFlag=false;
    waitForResult=false;
    error=false;
    ui->display->setText(firstOperator);

}

void Calculator::on_PB_equal_clicked()
{
    if (!error){
        QDateTime DT;
        double result;
        if (resultFlag){
            switch(op)
            {
                case '+':
                    result=firstOperator.toDouble()+secondOperator.toDouble();
                    break;
                case '-':
                    result=firstOperator.toDouble()-secondOperator.toDouble();
                    break;
                case 'x':
                    result=firstOperator.toDouble()*secondOperator.toDouble();
                    break;
                case '/':
                    result=firstOperator.toDouble()/secondOperator.toDouble();
                    break;
            }

            // Ввод в таблицу данных
            QString strF =
                      "INSERT INTO  HISTORY "
                      "VALUES('%1', '%2', '%3', '%4', '%5');";

            QString str = strF.arg(DT.currentDateTime().toString())
                              .arg(firstOperator)
                              .arg(op)
                              .arg(secondOperator)
                              .arg(QString::number(result));
            QSqlQuery query;
            if (!query.exec(str)) {
                qDebug() << "Unable to do insert opeation";
            }

            firstOperator=QString::number(result);
            ui->display->setText(firstOperator);

            waitForResult=false;
            dotFlag=false;
            firstOperatorFlag=true;
            resultFlag=true;
            memoryReadFlag=false;
        }
        if (waitForResult){

            switch(op)
            {
                case '+':
                    result=firstOperator.toDouble()+secondOperator.toDouble();
                    break;
                case '-':
                    result=firstOperator.toDouble()-secondOperator.toDouble();
                    break;
                case 'x':
                    result=firstOperator.toDouble()*secondOperator.toDouble();
                    break;
                case '/':
                    result=firstOperator.toDouble()/secondOperator.toDouble();
                    break;
            }

            // Ввод в таблицу данных
            QString strF =
                      "INSERT INTO  HISTORY "
                      "VALUES('%1', '%2', '%3', '%4', '%5');";

            QString str = strF.arg(DT.currentDateTime().toString())
                              .arg(firstOperator)
                              .arg(op)
                              .arg(secondOperator)
                              .arg(QString::number(result));
            QSqlQuery query;
            if (!query.exec(str)) {
                qDebug() << "Unable to do insert opeation";
            }

            firstOperator=QString::number(result);
            ui->display->setText(firstOperator);
            waitForResult=false;
            dotFlag=false;
            firstOperatorFlag=true;
            resultFlag=true;
            memoryReadFlag=false;
        }
    }
}

void Calculator::on_PB_percent_clicked()
{
    if (waitForResult){
        double tmp;
        tmp=firstOperator.toDouble()*secondOperator.toDouble()/100.0;
        secondOperator=QString::number(tmp);
        ui->display->setText(firstOperator+op+secondOperator);
    }
}

void Calculator::on_PB_backspace_clicked()
{
    if (!error & !resultFlag){
        if (firstOperatorFlag && firstOperator!=""){
            if (firstOperator[firstOperator.length()-1]=='.'){
                dotFlag=false;
            }
            firstOperator.chop(1);
            ui->display->setText(firstOperator);
            if (firstOperator==""){
                firstOperator="0";
                ui->display->setText(firstOperator);
            }
        }
        if (waitForResult && secondOperator!=""){
            if (secondOperator[secondOperator.length()-1]=='.'){
                dotFlag=false;
            }
            secondOperator.chop(1);
            ui->display->setText(firstOperator+op+secondOperator);
            if (secondOperator==""){
                secondOperator="0";
                waitForResult=false;
                ui->display->setText(firstOperator+op+secondOperator);
            }
        }
    }
}

void Calculator::on_PB_sqrt_clicked()
{
    if (firstOperatorFlag){
        if (firstOperator.toDouble()>=0.0){
            double tmp;
            tmp=qSqrt(firstOperator.toDouble());
            firstOperator=QString::number(tmp);
            ui->display->setText(firstOperator);
        }else{
            error=true;
            firstOperator="Err";
            ui->display->setText(firstOperator);
        }
    }else{
        if (waitForResult){
            if (secondOperator.toDouble()>=0.0){
                double tmp;
                tmp=qSqrt(secondOperator.toDouble());
                secondOperator=QString::number(tmp);
                ui->display->setText(firstOperator+op+secondOperator);
            }else{
                error=true;
                firstOperator="Err";
                ui->display->setText(firstOperator);
            }
        }
    }
}

void Calculator::on_PB_MS_clicked()
{
    if(!error){
        if (firstOperatorFlag){
            memory=firstOperator.toDouble();
            memorySaveFlag=true;
            ui->memSign->setVisible(true);
        }
        if (waitForResult){
            memory=secondOperator.toDouble();
            memorySaveFlag=true;
            ui->memSign->setVisible(true);
        }
    }
}

void Calculator::on_PB_MC_clicked()
{
    memory=0;
    memorySaveFlag=false;
    ui->memSign->setVisible(false);
}

void Calculator::on_PB_MR_clicked()
{
    if (!error && memorySaveFlag){
        if (firstOperatorFlag){
            firstOperator=QString::number(memory);
            ui->display->setText(firstOperator);
            memoryReadFlag=true;
        }else{
            secondOperator=QString::number(memory);
            ui->display->setText(firstOperator+op+secondOperator);
            memoryReadFlag=true;
            waitForResult=true;
        }
    }
}
