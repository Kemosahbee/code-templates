#include <iostream>
#include<cmath>

using namespace std;

int main()
{

    // Найти корни квадратного уравнения
    double a,b,c,D,x1,x2;
    cout << "Enter coeffitients of quadratic equation" << endl;
    cin >> a >> b >>c;
    D=b*b-4*a*c;
    if (D>=0){
        x1=(-b+sqrt(D))/(2*a);
        x2=(-b-sqrt(D))/(2*a);
        cout << "First root: x1=" << x1 << endl;
        cout << "second root: x2=" << x2 << endl;
    }else{
        cout << "Discriminant negative" << endl;
    }


    // Мини-калькулятор
    double Number1, Number2, result;
    char Sign;
    cout << "Enter number one" << endl;
    cin >> Number1;
    cout << "Enter operator" << endl;
    cin >> Sign;
    cout << "Enter number two" << endl;
    cin >> Number2;

    switch(Sign){
    case('+'):
        result=Number1+Number2;
        break;
    case('-'):
        result=Number1-Number2;
        break;
    case('*'):
        result=Number1*Number2;
        break;
    case('/'):
        result=Number1/Number2;
        break;
    }
    cout << Number1 << Sign << Number2 << "=" << result << endl;

    return 0;
}
