#include <iostream>
#include <conio.h>

using namespace std;

struct Auto
{
    char *car_brand;
    char *car_model;
    int year;
    double power;
};

void Input(Auto *inp)
{
    cout << "Enter car brand: ";
    inp->car_brand = new char[20];
    cin.get();
    cin.getline(inp->car_brand,20);

    cout << "Enter car model: ";
    inp->car_model = new char[20];
    cin.getline(inp->car_model,20);

    cout << "Enter year: ";
    cin >> inp->year;

    cout << "Enter power of car: ";
    cin >> inp->power;
}

void Free(Auto inp[], int n)
{
    for (int i(0); i<n; i++){
        delete [] inp[i].car_brand;
        delete [] inp[i].car_model;
    }
}

void ChooseAuto(const Auto *a)
{
    cout << "~~~Chosen car~~~" << endl;
    cout << "Brand: " << a->car_brand << endl;
    cout << "Model: " << a->car_model << endl;
    cout << "Year: " << a->year << endl;
    cout << "Power: " << a->power << endl;
}

void Print(const Auto *a, int length)
{
    for (int i(0); i< length; i++){
        cout << "~~~ " << i+1 << " car ~~~" << endl;
        cout << "Car brand: " << a[i].car_brand << endl;
        cout << "Car model: " << a[i].car_model << endl;
        cout << "Car year: " << a[i].year << endl;
        cout << "Car power: " << a[i].power << endl;
        cout << "------------------------\n" << endl;
    }
}

struct Time
{
    int hour;
    int minute;
};

Time InputTime()
{
    Time t;
    cout << "Enter the number of hours: ";
    cin >> t.hour;
    cout << "Enter the number of minutes: ";
    while (!(cin >> t.minute) || t.minute > 60){
        cin.clear();
        while (cin.get() != '\n');
        cout << "Wrong data. Enter proper value: ";
    }
    cout << t.hour << ":" << t.minute << endl;
    return t;
}

Time SumTime(const Time *t1, const Time *t2)
{
    Time s;
    s.hour = t1->hour+t2->hour;
    s.minute = t1->minute+t2->minute;
    if (s.minute>60){
        s.hour+=1;
        s.minute-=60;
    }
    return s;
}

int main()
{
    Time s1,s2,s,t[2];

//    s1=InputTime();
//    s2=InputTime();

    for (int i(0); i< 2; i++){
        t[i]=InputTime();
    }

    s = SumTime(&t[0], &t[1]);
    cout << "Sum of time: " << s.hour << ":" << s.minute << endl;
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
//
//    // Количество автомобилей которые будем вводить
//    int quantity;
//    cout << "Enter quantity of cars: " << endl;
//    cin >> quantity;
//
//    Auto *car = new Auto [quantity];
//
//    for (int i(0); i<quantity;  i++){
//        cout << i+1 << " car" << endl;
//        Input(&car[i]);
//    }
//
//    // Вывод всех введенных автомобилей
//    system("cls");
//    Print(car,quantity);
//    cout << endl;
//
//    // Вывод параметров выбранного автомобиля
//    cout << "~~~Select car number~~~" << endl;
//    int Number;
//    cin >> Number;
//    ChooseAuto(&car[Number-1]);
//
//    // Освобождаем память
//    delete [] car;
//    Free(car,quantity);
    _getch();
    return 0;
}
