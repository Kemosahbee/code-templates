#include <iostream>
#include <conio.h>
#include<math.h>
using namespace std;

int sum(int a, int b){
    int result=a+b;
    return result;
}

double my_abs(double a)
{
    if (a<0){
        a=-a;
    }
    double result = a;
    return result;
}

double abs_err(double a, double b)
{
    double abs_err=my_abs(a-b);
    return abs_err;
}

double relative_err(double a, double b)
{
    double relative_err = a/b;
    return relative_err;
}

long long int variation(int a, int b)
{
    long long int numerator, denomerator, result;
    numerator=1;
    denomerator=1;

    if (a>=b && a>0 && b>0){
    for(int i(1); i<=a; i++){
        numerator=i;
    }
    for(int i(1); i<=(a-b); i++){
        denomerator*=i;
    }
    result=numerator/denomerator;
    return result;
    }else{
        return -1;
    }
}

double sqrtsin(double a)
{
    double sqrtsin,result,tmp_num;
    long long int tmp_den;
    int tmp;
    result=0;

    for (int i(1); i<15; i=i+2){
        tmp_den=1;
        tmp_num=1;
        for (int j(1); j<=i; j++){
            tmp_den=tmp_den*j;
        }
        for (int k(1); k<=i; k++){
            tmp=1;
            for (int l(0); l<k-1; l++){
                tmp=tmp*(-1);
            }
            tmp_num=tmp_num*a*tmp;
        }
        result=result+tmp_num/tmp_den;
    }

    if (result>=0){
        sqrtsin=sqrt(result);
    }else{
        sqrtsin=-1;
    }
    return sqrtsin;

}

int main()
{
//    int a,b;
//    cout << "Enter two numbers: " << endl;
//    cin >> a >> b;

//    cout << "Sum of two numbers: " << endl;
//    cout << sum(a,b) << endl;

//    cout << "ABS of first number: " << endl;
//    cout << my_abs(a) << endl;

//    double n, m, absolute_error;
//    cout << "Measures: " << endl;
//    cin >> n >> m;
//    cout << "Absolute error: " << endl;
//    cout << abs_err(n,m) << endl;;
//    absolute_error=abs_err(n,m);
//    cout << "Relative error: " << endl;
//    cout << relative_err(absolute_error,n) << endl;

//    int k, l;
//    cout <<"Enter numbers for variations: " << endl;
//    cin >> k >> l;
//    cout << variation(k, l);

//    cout << "Enter degrees: " << endl;
//    double deg;
//    cin >> deg;
//    cout << sqrtsin(deg);

    _getch();
    return 0;
}
