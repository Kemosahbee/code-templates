#include <iostream>

using namespace std;

int main()
{
    int N, M, K, L;
    cout << "Enter N, M: " << endl;
    cin >> N >> M;

    int **A=new int*[N];
    for(int i(0); i<N; i++){
        A[i]=new int[M];
    }

    for(int i(0); i<N; i++){
        for(int j(0); j<M; j++){
            cin >> A[i][j];
        }

    }

    for(int i(0); i<N; i++){
        for(int j(0); j<M; j++){
             cout << A[i][j] << ' ';
        }
         cout << endl;
    }

    cout << "Enter K, L: " << endl;
    cin >> K >> L;

    int **B=new int*[K];
    for(int i(0); i<K; i++){
        B[i]=new int[L];
    }

    for(int i(0); i<K; i++){
        for(int j(0); j<L; j++){
            cin >> B[i][j];
        }
    }

    for(int i(0); i<K; i++){
        for(int j(0); j<L; j++){
             cout << B[i][j] << ' ';
        }
         cout << endl;
    }

    if (M!=K){
        cout << "Impossible to multiply matrices";
    }else{
        int **C=new int*[N];
        for(int i(0); i<N; i++){
            C[i]=new int[L];
        }

        for(int i(0); i<N; i++){
            for(int j(0); j<M; j++){
                 C[i][j]=0;
            }
        }

        for(int i(0); i<N; i++){
            for(int j(0); j<L; j++){
                for(int r(0); r<M; r++){
                    C[i][j]+=A[i][r]*B[r][j];
                }
                cout << C[i][j] <<' ';
            }
            cout << endl;
        }

        for(int i(0); i<N; i++){
            delete [] C[i];
        }
        delete [] C;
    }



    for(int i(0); i<N; i++){
        delete [] A[i];
    }
    delete [] A;

    for(int i(0); i<K; i++){
        delete [] B[i];
    }
    delete [] B;


    getchar();
    return 0;
}
