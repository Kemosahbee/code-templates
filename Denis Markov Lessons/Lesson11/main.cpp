#include <iostream>
#include <conio.h>
#include <random>
#include <time.h>

using namespace std;

char name1[30];
char name2[30];
char table[3][3];
bool step;

void instruction()
{
    cout << "\t\t*** Tic tac toe***\n\n";
    cout << "Rules:\n";
    cout << "Two players play on the 3x3 field\n";
    cout << "The winner is the one who collects the winning combination\n";
    cout << "Field view\n\n";
    int l=0;

    for(int i(0); i<3; i++)
    {
        for(int j(0); j<3; j++)
        {
            cout << "| " << l+1 << ' ';
            table[i][j]=char(49+l);
            l++;
        }
        cout << '|';
        cout << endl;
    }
    cout << "\nFor the move, press the digit of the field\n";
    cout << "Press key for the start game:";
    _getch();
}

bool input()
{
    for(int i(0); i<3; i++){
        for(int j(0); j<3; j++){
            cout << "| " << table [i][j] << ' ';
        }
        cout << '|';
        cout << endl;
    }

    cout << endl;
    if(step){
        cout << name1 << " turn" << " : ";
    }else{
        cout << name2 << " turn" << " : ";
    }

    char n;
    cin >> n;
    if(n<'1' || n>'9'){
        return false;
    }

    int i,j;

    if((n-'0')%3==0){
        i=(n-'0')/3-1;
        j=2;
    }else{
        j=(n-'0')%3-1;
        i=(n-'0')/3;
    }

    if (table[i][j] == '0' || table[i][j] == 'X'){
        return false;
    }

    if(step){
        table[i][j] = 'X';
        step = false;
    }else{
        table[i][j]='0';
        step = true;
    }
    return true;

}

bool win()
{
    for(int i(0); i<3; i++){
        if ((table[i][0]==table[i][1]) && (table[i][0]==table[i][2])){
            return true;
        }else if ((table[0][i]==table[1][i]) && (table[0][i]==table[2][i])){
            return true;
        }else if ((table[0][0]==table[1][1] && table[0][0]==table[2][2]) || (table[0][2]==table[1][1] && table[0][2]==table[2][0])){
            return true;
        }
    }
    return false;
}

int main()
{
    instruction();
    system("cls");

    cout << "Enter 1st player name: ";
    cin.getline(name1,30);

    cout << "Enter 2nd player name: ";
    cin.getline(name2,30);


    int k=0;
    for (int i(0); i<30; i++){
        if (name1[i] == name2[i]){
            k+=1;
        }
    }
    if (k==30){
        cout << name2 << " already engaged, change name:";
        cin.getline(name2,30);
    }


    srand(time(NULL));
    if (rand() & 1){
        step = true;
    }else{
    step = false;
    }

    int g=0;
    while(!win())
    {
        system("cls");
        if (!input()){
            cout << "You enter wrong data, please repeat!";
            _getch();
        }else{
            g=g+1;
        }
        if (g==9 && !win()){
            system("cls");
            cout << "Dawn!" << endl;
            _getch();
            return 0;
        }
    }
    system("cls");
    if(step){
        cout << name2 << " won!" << endl;
    }else{
        cout << name1 << " won!" << endl;
    }
    _getch();
    return 0;
}
