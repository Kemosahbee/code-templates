#include <iostream>
#include<cmath>

using namespace std;

int main()
{
    //Вывод таблицы степеней двойки n-раз
    int n;
    cout <<"Enter power of two" << endl;
    cin >> n;
    for(int i;i<n;i++){
        cout << pow(2,i) << endl;
    }

    //Посчитать НОК и НОД
    int a,b,tmp,NOD,NOK,product;
    cout << "Enter 2 numbers" << endl;
    cin >> a >> b;
    product=a*b;
    if (a<=0 || b<=0){
        cout << "Incorrect input" << endl;
    }else{
        if (a==b){
            NOD=a;
        }
        while (a!=b){
            if (a>b){
                tmp=a;
                a=b;
                b=tmp;
            }
            b=b-a;
        }
        NOD=b;
    }
    NOK=product/NOD;

    cout << "Greatest common divisor: " << NOD << endl;
    cout << "Least common multiple: " << NOK << endl;


    //Вывести строки
    for(int i=1; i<=n; i++){
        for (int j=1; j<=i;j++){
            cout << '*';
        }
        cout << endl;
    }


    return 0;
}
