//Lesson3

#include<iostream>
using namespace std;

int main()
{
    //Ввести секунды. Посчитать сколько это часов, минут и секунд
    cout << "Task 1" << endl;
    int Seconds;
    cout << "Enter amount of seconds" << endl;
    cin >> Seconds;
    cout << Seconds << " seconds " << " equal " << Seconds/3600 << " Hours " << (Seconds-(Seconds/3600)*3600)/60 << " Minutes " << (Seconds-(Seconds/3600)*3600)%60 << " Seconds" << endl;

    //Ввести количество рублей и цену товара. Посчитать сколько можно купить на эти деньги.
    cout << "Task 2" << endl;
    int Rubles, Price;
    cout << "Enter amount of rubles" << endl;
    cin >> Rubles;
    cout << "Enter price" << endl;
    cin >> Price;
    cout << "You may buy " << Rubles/Price << " units" << endl;

    //Ввести два числа. Найти в процентном соотношении 1 от 2
    cout << "Task 3" << endl;
    double Number1, Number2;
    cout << "Enter two numbers " << endl;
    cin >> Number1 >> Number2;
    cout << Number1/Number2*100 << " percent" << endl;
    return 0;
}
