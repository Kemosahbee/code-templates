#include "mystring.h"
#include <list>

using std::cout;
using std::endl;

bool comparer (const MyString &s1, const MyString &s2);

int main()
{
    MyString s1;
    MyString s2 ("Targem!");
    s1 = "Hello, ";
    cout << s1 << s2 << endl;
    cout << "Again" << endl;
    MyString s3 = s1 + s2;
    cout << s3 << endl;
    cout << endl;

    std::list<MyString> mylist;
    std::list<MyString>::iterator it;

    mylist.push_back(s3);
    mylist.push_back("target");
    mylist.push_back("Targem");
    mylist.push_back("targem");
    mylist.push_back("Targem games");
    mylist.push_back("Targem Games.");
    mylist.push_back("TARGEM");

    cout << "Unsorted list:" << endl;
    for (it=mylist.begin(); it!=mylist.end(); it++)
    {
        cout << ' ' << *it << endl;
    }

    mylist.sort(comparer);

    cout << "Sorted list:" << endl;
     for (it=mylist.begin(); it!=mylist.end(); it++)
    {
        cout << ' ' << *it << endl;
    }
    std::cin.get();
    return 0;
}

bool comparer (const MyString &s1, const MyString &s2)
{
    unsigned int i=0;
    while (i<s1.Length() && i<s2.Length())
    {
        if (tolower(s1[i])<tolower(s2[i]))
        {
            return 0;
        }else if (tolower(s1[i])>tolower(s2[i]))
        {
            return 1;
        }
        i++;
    }
    return s1.Length()>s2.Length();
}