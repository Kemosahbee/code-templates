#ifndef MYSTRING_H_
#define MYSTRING_H_
#include <iostream>
class MyString
{
    private:
        char *str;
    public:
        MyString();
        MyString(const MyString &s);
        MyString(const char *s);
        ~MyString();
        unsigned int Length() const;
        MyString &operator=(const MyString &s);
        MyString &operator=(const char *s);
        char &operator[](int i) const;
        friend bool operator<(const MyString &s1, const MyString &s2);
        friend bool operator>(const MyString &s1, const MyString &s2);
        friend bool operator==(const MyString &s1, const MyString &s2);
        friend std::ostream &operator<<(std::ostream &os, const MyString &s);
        friend MyString operator+(const MyString &s1, const MyString &s2);
        friend MyString operator+(const char *s1, const MyString &s2);
        friend MyString operator+(const MyString &s1, const char *s2);
};
#endif