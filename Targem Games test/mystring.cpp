#include <cstring>
#include "mystring.h"

MyString::MyString()
{
    str = new char[1];
    str[0]='\0';
}

MyString::MyString(const MyString &s)
{
    str = new char[strlen(s.str)+1];
    strcpy(str, s.str);
}

MyString::MyString(const char *s)
{
    str = new char[strlen(s)+1];
    strcpy(str, s);
}

MyString::~MyString()
{
    delete [] str;
}

unsigned int MyString::Length() const
{
    return strlen(str);
}

char &MyString::operator[](int i) const
{
    return str[i];
}

MyString & MyString::operator=(const MyString &s)
{
    if (this==&s){
        return *this;
    }
    delete [] str;
    str = new char [strlen(s.str)+1];
    strcpy(str, s.str);
    return *this;
}

MyString & MyString::operator=(const char *s)
{
    delete [] str;
    str = new char [strlen(s)+1];
    strcpy(str, s);
    return *this;
}

MyString operator+(const MyString &s1, const MyString &s2)
{
    MyString sum;
    sum.str = new char[strlen(s1.str)+strlen(s2.str)+1];
    strcpy(sum.str, s1.str);
    strcat(sum.str, s2.str);
    return sum;
}

MyString operator+(const MyString &s1, const char *s2)
{
    MyString sum;
    sum.str = new char[strlen(s1.str)+strlen(s2)+1];
    strcpy(sum.str, s1.str);
    strcat(sum.str, s2);
    return sum;
}

MyString operator+(const char *s1, const MyString &s2)
{
    return s2+s1;
}

std::ostream &operator<<(std::ostream &os, const MyString &s)
{
    os << s.str;
    return os;
}

bool operator<(const MyString &s1, const MyString &s2)
{
    return (strcmp(s1.str, s2.str)<0);
}

bool operator>(const MyString &s1, const MyString &s2)
{
    return s2.str<s1.str;
}

bool operator==(const MyString &s1, const MyString &s2)
{
    return strcmp(s1.str, s2.str)==0;
}